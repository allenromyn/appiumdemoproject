package com.test.sitemanager.lmg710;


import org.openqa.selenium.By;
import org.testng.annotations.Test;




public class SMMobileEmailLoginTests extends SMLGG7ThinQ_LMG710BaseClass {
//public class SMMobileEmailLoginTests extends SMSamsungGalaxyG9BaseClass {
// public class SMMobileEmailLoginTests extends SMHuaweiBaseClass {
		
	
	@Test
	public void mobileEmailLoginTests() {
	
		logotap();
		System.out.println("Starting SMMobileEmailLoginTests.");
		sleep(4000);

		

		// Select and change to dev environment popup
		sleep(1900);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

		// Click dev change to dev environment
		driver.findElement(By.id("android:id/button1")).click();

		// select login element
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();

		// enter email
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		System.out.println("login..!");

		// Skip tutorial
		sleep(5000);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(5000);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(5000);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

	
		sleep(2000);
		System.out.println("End SMMobileEmailLoginTests.");
	
		
			

		}

	
	}


 
 
