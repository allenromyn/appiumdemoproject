package com.tests.RemoteAccess;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RemoteDefaultMobileShareUnitTests extends RemoteDefaultBaseClass {

	@Test
	public void remoteDefaultMobileBottomMenuTests() {
		logotap();
		sleep(2000);
		System.out.println("Starting RemoteDefaultMobileBottomMenuTests.");
	

		// Select and change to dev environment popup
		sleep(2800);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();

		// select login element
		sleep(1800);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();

		// enter email
		sleep(1800);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1000);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		System.out.println("login..!");

		// Skip tutorial
		sleep(4800);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(4800);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4800);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(4800);

		
		// Select Unit menu from bottom
		MobileElement unitmenu = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView"));
		unitmenu.click();
		System.out.println("Select Units from menu.. !");
		sleep(2800);
		
		
		// Select share unit plus sign upper right hand corner
		MobileElement unitplus = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"add button\"]"));
		unitplus.click();
		System.out.println("Select plus sign .. !");
		sleep(2800);
		
		// Select share Unit
		MobileElement shareunitone = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView"));
		shareunitone.click();
		System.out.println("Select share unit .. !");
		sleep(2800);
		
		// Eneter phone number to share with
		MobileElement enterphonetoshare = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.EditText"));
		enterphonetoshare.click();
		enterphonetoshare.clear();
		enterphonetoshare.sendKeys("8018889999");
		System.out.println("Enter number .. !");
		sleep(2800);
		
	
		
		// Select arrow to select unit
		MobileElement arrowunit = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView"));
		arrowunit.click();
		System.out.println("Select arrow .. !");
		sleep(2800);
	
		
		
		// Select first unit in list
		MobileElement firstunitlist = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView"));
		firstunitlist.click();
		System.out.println("Select Unit to share .. !");
		sleep(2800);
		
		
		
		
		// Select yes to share.
		MobileElement selectyes = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]"));
		selectyes.click();
		System.out.println("Select Yes to share .. !");
		sleep(5500);
		
		
		
		
	
		// Exit to home page
		homepagetoexit();

		sleep(1000);
		System.out.println("End RemoteDefaultMobileBottomMenuTests.");

	}

}
