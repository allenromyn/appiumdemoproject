package com.tests.RemoteAccess;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RemoteSMMobileActivitySortingTests extends RemoteSMBaseClass {
	static String sunit = "3013";
	
	@Test
	public void remoteSMMobileActivitySortingTests() {

		logotap();
		sleep(2000);
		System.out.println("Starting RemoteSMMobileActivitySortingTests.");
		
		  // Select and change to dev environment popup
		sleep(1800);
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		  
		  // Click dev change to dev environment 
		  MobileElement changetodev = driver.findElement(By.id("android:id/button1")); changetodev.click();
		  
		 
		// select login element
		sleep(1800);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();
		
				
		// enter email
		sleep(1800);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " +  em );
		
		
		
		// enter password
		sleep(1800);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		
		
		
		// Skip tutorial 
		sleep(4500);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");
		
		// Select and set location popup
		sleep(4500);
		MobileElement widgetclick2 = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");
	
		// Select Allow access
		sleep(4500);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");
		

		
		
		// Select Activity
		sleep(4500);
		MobileElement selectactivity = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView"));
		selectactivity.click();
		System.out.println("Select Activity column.. !");
		
		
		// Select Activity - Units column
		sleep(4500);
		MobileElement selectactivityunits = driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView"));
		selectactivityunits.click();
		System.out.println("Select Units column.. !");
		
		
		// Select Activity - Share column
		sleep(4500);
		MobileElement selectsharecol = driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView"));
		selectsharecol.click();
		System.out.println("Select Share column.. !");
			
	
		
	
			
			// Mobile app logout from lower menu.
			sleep(1000);
			System.out.println("End RemoteSMMobileActivitySortingTests.");
		

	}
}


