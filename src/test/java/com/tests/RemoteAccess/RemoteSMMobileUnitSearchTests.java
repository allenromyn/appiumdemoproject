package com.tests.RemoteAccess;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RemoteSMMobileUnitSearchTests extends RemoteSMBaseClass {
	static String sunit = "3013";
	
	@Test
	public void remoteSMMobileUnitSearchTests() {

		logotap();
		sleep(2000);
		System.out.println("Starting RemoteSMMobileUnitSearchTests.");
		
		
	
		
		  // Select and change to dev environment popup
			sleep(1800);
			MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
			widgetclick.click();
			  
			  // Click dev change to dev environment 
			  MobileElement changetodev = driver.findElement(By.id("android:id/button1")); changetodev.click();
			  
			 
			// select login element
			sleep(1800);
			MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
			mailicon.click();
			
					
			// enter email
			sleep(1800);
			MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
			emailenter.sendKeys(em);
			System.out.println("Enter email as: " +  em );
			
			
			
			// enter password
			sleep(1800);
			MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
			password.sendKeys(pw);
			System.out.println("Enter password.");
			
			
			// login arrow
			MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
			loginarrow.click();
			
			
			
			// Skip tutorial 
			sleep(3000);
			MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
			skiptutorial.click();
			System.out.println("Skip tutorial.");
			
			// Select and set location popup
			sleep(3000);
			MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
			widgetclick2.click();
			System.out.println("Select Ok accept location popup.");
		
			// Select Allow access
			sleep(3000);
			MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
			allowaccesslocation.click();
			System.out.println("Allow access to application.. !");
			

			
			// Change to Noke Office Test
			changenokeofficetest();

		
			
			// Select Units page
			sleep(3000);
			MobileElement unitspage = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView"));
			unitspage.click();
			System.out.println("Select Unit page.. !");
			
			// Select seatch spy glass
			sleep(3000);
			MobileElement searchspyglass = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]"));
			searchspyglass.click();
			System.out.println("Select spy glass search.. !");
			
			
			// Select Enter searcj criteria 
			sleep(3000);
			MobileElement entersearchunit = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText"));
			entersearchunit.sendKeys(sunit);
			System.out.println("Enter unit to search for.. !");
			
			
			// Select Enter searcj criteria 
			sleep(3000);
			MobileElement unitinfo = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView"));
			unitinfo.click();
			System.out.println("View unit info.. !");
			sleep(3000);
			driver.navigate().back();
			sleep(3000);
			driver.navigate().back();
			
			
			
			
			// Mobile app logout from lower menu.
			sleep(1000);
			System.out.println("End RemoteSMMobileUnitSearchTests.");
		

	}
}


