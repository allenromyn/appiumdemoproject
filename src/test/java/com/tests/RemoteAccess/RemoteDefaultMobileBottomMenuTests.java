package com.tests.RemoteAccess;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RemoteDefaultMobileBottomMenuTests extends RemoteDefaultBaseClass {

	@Test
	public void remoteDefaultMobileBottomMenuTests() {
		logotap();
		sleep(2000);
		System.out.println("Starting RemoteDefaultMobileBottomMenuTests.");
	
		
		
		
		  // Select and change to dev environment popup
		sleep(1800);
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		  
		  // Click dev change to dev environment 
		  MobileElement changetodev = driver.findElement(By.id("android:id/button1")); changetodev.click();
		  
		 
		// select login element
		sleep(1800);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();
		
				
		// enter email
		sleep(1800);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " +  em );
		
		
		
		// enter password
		sleep(1800);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		
		
		
		// Skip tutorial 
		sleep(7000);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");
		
		// Select and set location popup
		sleep(7000);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");
	
		// Select Allow access
		sleep(7000);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");
		

		
		// Select Units tab
		sleep(7000);
		MobileElement unitstab = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView"));
		unitstab.click();
		System.out.println("Selected Units tab bottom menu..!");
		
		
		// Select Users tab.
		sleep(7000);
		MobileElement usertab = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView"));
		usertab.click();
		System.out.println("Selected Users tab bottom menu..!");
		
		
		// Select Activity tab.
		sleep(7000);
		MobileElement activitytab = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView"));
		activitytab.click();
		System.out.println("Selected Activity tab bottom menu..!");
			
		
		
		// Select Home tab.
		sleep(7000);
		MobileElement hometab = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView"));
		hometab.click();
		System.out.println("Selected Home tab bottom menu..!");
		
		
		
		sleep(1000);
		System.out.println("End RemoteDefaultMobileBottomMenuTests.");
	
	
	}
	
}


