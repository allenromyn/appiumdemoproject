package com.tests.RemoteAccess;

import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;

public class RemoteDefaultBaseClass {

	static int tc = 13;
	static String em = "Default@noke.com";
	static String pw = "password";
	static String si = "Noke Office Test";
	// special parameters
	static String sunit = "3013";
	static String cmdwake = "adb shell input keyevent 26";
	static String cmdsleep = "adb shell input keyevent 26";
	static String cmdUnlock = "adb shell input keyevent 82";

	static String ip = "10.2.13.19:5555"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "SMG965U"; // device name
	static String uid = "3431313949413098"; // UDID device id
	static String pn = "ANDROID";

	AppiumDriver<MobileElement> driver;

	// Setup starts server , setsup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
		
		System.out.println("Starting serve..!");
		
		  sleep(3000); 
		  Runtime.getRuntime().exec(cmdwake);
		  //Runtime.getRuntime().exec(cmdwake);
		  
		  System.out.println("Waking phone up..!"); 
		  sleep(3000);
		  Runtime.getRuntime().exec(cmdUnlock); 
		  //Runtime.getRuntime().exec(cmdUnlock);
		  System.out.println("Unlocking ready to test..!");
		 

		
		 {
			    CommandLine cmd = new CommandLine("C:\\Program Files (x86)\\Appium\\node.exe");
			    cmd.addArgument("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\Appium.js");
			    cmd.addArgument("--address");
			    cmd.addArgument("127.0.0.1");
			    cmd.addArgument("--port");
			    cmd.addArgument("4725");

			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(9999);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			}
		

		sleep(3999);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, ip);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability("appActivity", "com.noke.storagesmartentry.ui.login.LoginActivity");
			

			URL url = new URL("http://127.0.0.1:4725/wd/hub");

						
			driver = new AppiumDriver<MobileElement>(url, caps);
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
		}

	}


	
	
	// logout , stop servers.
	@AfterTest
	public void teardown() throws IOException {
		
		  // Mobile app logout from lower  menu. // 
		  System.out.println("Logout .. and reset application..!");
		  System.out.println("Reset App .. !"); //
		  driver.resetApp(); 
		  sleep(3000); 
		  driver.closeApp(); // 
		  sleep(3000); //
		  driver.runAppInBackground(Duration.ofSeconds(0)); // 
		  sleep(3000); //
		  driver.quit();
		  
		
		  Runtime.getRuntime().exec(cmdsleep);
		  System.out.println("Android Asleep .. !"); 
		  sleep(4000);
		 
		{
					}
		{
			
			
			Runtime runtime = Runtime.getRuntime();
			try {
				
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("===============================================");
				sleep(3000);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	
	
	
	
	
	public void logotap() {
		sleep(2000);
		for (int i = 0; i < tc; i++) {
			// click the button
			MobileElement logochangetodev = driver.findElement(By.id("com.noke.nokeaccess:id/main_logo"));
			logochangetodev.click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
				
				
	}
	
	
	// Change to Noke Office Teat
	public void changenokeofficetest() {
		// Change to store Noke Office Test.
		sleep(4500);
		MobileElement nokeofficetest = driver.findElement(By.id("com.noke.nokeaccess:id/switch_site_button"));
		nokeofficetest.click();
		System.out.println("Change to Noke Office Test.. !");

		// Enter company store - change to Noke Office test
		sleep(4500);
		MobileElement enternokeofficetest = driver.findElement(By.id("com.noke.nokeaccess:id/title"));
		enternokeofficetest.click();
		System.out.println("Select Noke Office Test.. !");
		sleep(2000);
		System.out.println("Changed to Noke Office Test.. !");
	
	}
	
	
	public void homepagetoexit() {
		// Go to home page to exit 
		MobileElement selecthome = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView"));
		selecthome.click();
		System.out.println("Home page .. !");
		sleep(4500);
	}

		

	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
