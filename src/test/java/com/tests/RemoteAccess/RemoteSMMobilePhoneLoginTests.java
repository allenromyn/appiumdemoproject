package com.tests.RemoteAccess;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RemoteSMMobilePhoneLoginTests extends RemoteSMBaseClass {
	
	
	@Test
	public void remoteSMMobileEmailLoginTests() {

		logotap();
		sleep(2000);
		System.out.println("Starting RemoteSMMobileEmailLoginTests.");
		
		// Select and change to dev environment popup
		sleep(1800);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();
		System.out.println("Environment changed.");
		
		
		// select login element email 
		sleep(1000);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();
		System.out.println("eMail icon selected.");

	
		
		// select phone button
		sleep(1000);
		MobileElement phonebutton = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.ImageView[2]"));
		phonebutton.click();
		System.out.println("Phone icon selected.");

	
		// select phone enter text line
		sleep(1500);
		MobileElement phoneinputline = driver.findElement(By.id("com.noke.nokeaccess:id/phone_edit_text"));
		phoneinputline.clear();
		sleep(1000);
		phoneinputline.sendKeys(ph);
		System.out.println("Entetr phone."+ ph + "  ..!" );
	
		// enter password
		sleep(3000);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		sleep(2000);
		MobileElement loginarrow = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Show password\"]"));
		loginarrow.click();
		System.out.println("login ..!" );

		// Skip tutorial
		sleep(3000);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(3000);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(3000);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

		
		// Change to Noke Office Test
		changenokeofficetest();

			
			
		// Mobile app logout from lower menu.
		sleep(3000);
		System.out.println("End RemoteSMMobileEmailLoginTests.");

	}
}


