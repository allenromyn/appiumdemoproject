package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class DefaultMobileUnShareUnitTests extends DefaultBaseClass {

	@Test
	public void defaultMobileUnShareUnitTests() {
		logotap();
		sleep(2800);
		System.out.println("Starting DefaultMobileUnShareUnitTests.");
	
		// Select and change to dev environment popup
		sleep(2800);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

		// Click dev change to dev environment
		driver.findElement(By.id("android:id/button1")).click();

		// select login element
		sleep(1800);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();

		// enter email
		sleep(1800);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1000);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		System.out.println("login..!");

		// Skip tutorial
		sleep(4800);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(4800);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");
		// Select Allow access
		sleep(4800);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(4800);

		
		// Select Unit menu from bottom
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from bottom menu.. !");
		sleep(2900);
		
		
		
		// Select unit to unshare 
		// This is the unit that was shared in DefaultMobileShareUnitTests
		// Select Unit menu from bottom
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Select A shared Unit.. !");
		sleep(2900);
		
		// Select users of shared unit to view and unshare
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Users to unshare.. !");
		sleep(2900);
		
		// Select User to remove from list
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Select User to remove.. !");
		sleep(2900);
		
		// Select Remove
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.TextView\r\n")).click();
		System.out.println("Select Remove .. !");
		sleep(5900);
		
		// Select yes to popup
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		System.out.println("Select yes from popup .. !");
		sleep(9000);
		driver.navigate().back();
		driver.navigate().back();
		
		
		// view unit 
		// Select Unit menu from bottom
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from bottom menu.. !");
		sleep(2800);

		// Select specific unit to show no users being shared
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Select specific unit to view no shares.. !");
		sleep(2800);
		
		// Select users of shared unit to view and unshare
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Users to view unshared.. !");
		sleep(2900);
		driver.navigate().back();
		sleep(2900);
		driver.navigate().back();
		sleep(2900);
		
		// Exit to home page
		homepagetoexit();

		sleep(1000);
		System.out.println("End DefaultMobileUnShareUnitTests.");

	}

}
