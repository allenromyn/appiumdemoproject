package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class DefaultMobileEmailLoginTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileEmailLoginTests";
	
	@Test
	public void defaultMobileEmailLoginTests() {

		
		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);

		
		// Select and change to dev environment popup
		sleep(2500);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();
		  
		  // Click dev change to dev environment 
		 driver.findElement(By.id("android:id/button1")).click();
		  
		 
		// select login element
		sleep(2500);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();
		
				
		// enter email
		sleep(2500);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " +  em );
		
		
		
		// enter password
		sleep(2500);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		
		
		
		// Skip tutorial 
		sleep(4000);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");
		
		// Select and set location popup
		sleep(4000);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");
	
		// Select Allow access
		sleep(4000);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");
		


		// Change to Noke Office Test
		changetoNokeOfficeTest();
		
		// Exit to home page
		sleep(3000);
		homepagetoexit();

		
		// Mobile app logout from lower menu.
		System.out.println("Ending :" + curmod + " test.");
		sleep(4999);

	
	
	}
}
