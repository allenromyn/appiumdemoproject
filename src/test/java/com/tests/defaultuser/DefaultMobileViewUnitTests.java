package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class DefaultMobileViewUnitTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileViewUnitTests"; 
	
	
	@Test
	public void defaultMobileViewUnitTests() {

		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);

	
		// Skip tutorial
		sleep(2900);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(2900);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(2900);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		// Change to Noke Office Test
		changetoNokeOfficeTest();

		// view unit on home page
		sleep(2900);
		driver.findElement(By.id("com.noke.nokeaccess:id/entries_icon")).click();
		System.out.println("View units page from home page.. !");
		// exit unit page back to home page
		sleep(2900);
		driver.findElement(By.id("com.noke.nokeaccess:id/close")).click();
		System.out.println("Exit units page .. !");

		System.out.println("Site Manager Home Page..!");
		sleep(2900);

		// Access exit page menu
		sleep(2900);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView"))
				.click();
		System.out.println("Select bottom Menu To Exit page .. !");

		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*********************************");
		sleep(4999);

	}

}

