package com.tests.defaultuser;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

public class DefaultMobileHomePageHelpSupportTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileHomePageHelpSupportTests";
	
	
	@Test
	public void defaultMobileHomePageHelpSupportTests() {


		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);


		// skip tutorial 
		skipTutorial();

		// Select and set location popup
		sleep(4500);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4500);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		// Change to Noke Office Test. Located in BaseClass. Changes to Noke Office
		// test.
		changetoNokeOfficeTest();

		System.out.println("DefaultUser Home Page..!");
		sleep(4500);

		// Select Units from lower menu.
		sleep(2000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from home page .. !");

		// Access Help and Support
		sleep(3000);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select Question Mark = Help & Support Link  .. !");

		// Access Help and Support View Tenant Support start here.
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView")).click();
		System.out.println("Select View Tenant Support-Start Here Link  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Open Gate Door.
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView")).click();
		System.out.println("Select View Tenant Support-Open Gate Door Link  .. !");
		sleep(3000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Unlocking your unit
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]")).click();
		System.out.println("Select View Tenant Support-Unlocking Your Unit  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support locking your unit
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]")).click();
		System.out.println("Select View Tenant Support-Locking Your Unit  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Didgital Key
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]")).click();
		System.out.println("Select View Tenant Support-Sharing a Digital Key  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Make a Payment
		sleep(3000);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]")).click();
		System.out.println("Select View Tenant Support-Making A Payment Not funtional for now  .. !");
		sleep(2000);

		// Click ok to close popup
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")).click();
		System.out.println("Select View Tenant Support-Exit Make a payment error popup .. !");
		sleep(2000);
		driver.navigate().back();

		// Return home before exit application
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView")).click();
		System.out.println("Return to home page before application exit... !");
		sleep(2000);
		
		
		
		// Mobile app logout from lower menu.
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*********************************");
		sleep(4999);
		
	
		// Exit to home page
		sleep(4000);
		homepagetoexit();
	
		

	}

	public static void exitFilterPage() {
		MobileElement selectfiltersxout = driver.findElement(By.id("com.noke.nokeaccess:id/close"));
		selectfiltersxout.click();
		System.out.println("Exit Filter page.. !");
		sleep(4000);
	}

}
