package com.tests.defaultuser;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class DefaultMobileViewTutorialsTests extends DefaultBaseClass {

	@Test
	public void defaultMobileViewTutorialsTests() {

		logotap();
		sleep(2000);
		System.out.println("Starting DefaultMobileViewTutorialsTests");

		// Select and change to dev environment popup
		sleep(1750);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();

		// select login element
		sleep(1750);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();

		// enter email
		sleep(1750);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1750);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		System.out.println("login..!");

		// View first - second page of tutorial
		sleep(4500);
		System.out.println("Review first page of Tutorial..!");
		MobileElement firstpagetutorialnexgt = driver.findElement(By.id("com.noke.nokeaccess:id/next_text"));
		firstpagetutorialnexgt.click();
		System.out.println("Review second Search tutorial..!");

		// View third page of tutorial
		sleep(4500);
		MobileElement thirdpagetutorialnexgt = driver.findElement(By.id("com.noke.nokeaccess:id/next_text"));
		thirdpagetutorialnexgt.click();
		System.out.println("Review third Units and Entries tutorial..!");

		// View forth page of tutorial
		sleep(4500);
		MobileElement forthpagetutorialnexgt = driver.findElement(By.id("com.noke.nokeaccess:id/next_text"));
		forthpagetutorialnexgt.click();
		System.out.println("Review forth page Add Fobs and Units tutorial..!");

		// View fith page of tutorial
		sleep(4500);
		MobileElement fithpagetutorialnexgt = driver.findElement(By.id("com.noke.nokeaccess:id/next_text"));
		fithpagetutorialnexgt.click();
		System.out.println("Review Fith  page More Settings  tutorial..!");

		// Done with tutorial
		sleep(4500);
		MobileElement finishedexit = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
		finishedexit.click();
		System.out.println("Review Fith  page More Settings  tutorial..!");

		// Select and set location popup
		sleep(5000);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(5000);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

			System.out.println("Site Manager Home Page..!");
		sleep(2900);
		System.out.println("End DefaultMobileViewTutorialsTests");

	}
}
