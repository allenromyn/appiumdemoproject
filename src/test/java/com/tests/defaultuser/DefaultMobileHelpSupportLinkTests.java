package com.tests.defaultuser;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class DefaultMobileHelpSupportLinkTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileHelpSupportLinkTests";
	
	
	@Test
	public void defaultMobileHelpSupportLinkTests() {

		sleep(2999);
		System.out.println("Starting :" + curmod + " test.");



		// skip tutorial 
		skipTutorial();

		// Select and set location popup
		sleep(4500);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4500);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		// Change to Noke Office Test
		changetoNokeOfficeTest();

		System.out.println("DefaultUser Home Page..!");
		sleep(4500);

		sleep(4500);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView"))
				.click();
		System.out.println("Select bottom Menu To Exit page .. !");

		// Access Help and Support
		sleep(4500);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView"))
				.click();
		System.out.println("Select Help & Support Link  .. !");

		// Access Help and Support View Tenant Support start here.
		sleep(4500);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView"))
				.click();
		System.out.println("Select View Tenant Support-Start Here Link  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Open Gate Door.
		sleep(4500);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView"))
				.click();
		System.out.println("Select View Tenant Support-Open Gate Door Link  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Unlocking your unit
		sleep(4000);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]"))
				.click();
		System.out.println("Select View Tenant Support-Unlocking Your Unit  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support locking your unit
		sleep(4000);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]"))
				.click();
		System.out.println("Select View Tenant Support-Locking Your Unit  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Didgital Key
		sleep(4000);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]"))
				.click();
		System.out.println("Select View Tenant Support-Sharing a Digital Key  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Make a Payment
		sleep(4000);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]"))
				.click();
		System.out.println("Select View Tenant Support-Making A Payment Not funtional for now  .. !");
		sleep(2000);

		// Click ok to close popup
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button"))
				.click();
		System.out.println("Select View Tenant Support-Exit Make a payment error popup .. !");
		sleep(2000);
		driver.navigate().back();

		// Exit to home page
		homepagetoexit();

		sleep(2999);
		System.out.println("Ending :" + curmod + " test.");

	}

}
