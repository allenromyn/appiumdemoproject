package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class DefaultMobileAboutTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileAboutTests";
	
	
	@Test
	public void dDefaultMobileAboutTests() {
		
		
		
		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);



		// Skip tutorial
		sleep(3999);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(3999);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(3999);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		
		
		// Select lower menu 
		sleep(3999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select lower meun.. !");
		

		// Select About
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("Select About.. !");
		sleep(3999);
		driver.navigate().back();
		
			
		// Exit to home page
		sleep(3999);
		homepagetoexit();
				

		
		// Mobile app logout from lower menu.
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*********************************");
		sleep(4999);
	
	
	}
}
