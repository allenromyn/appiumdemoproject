package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;



public class DefaultMobilePhoneLoginPageRefreshTests extends DefaultBaseClass {

	@Test
	public void defaultMobilePhoneLoginPageRefreshTests() {

		
		logotap();
		sleep(2000);
		System.out.println("Starting DefaultMobilePhoneLoginPageRefreshTests.");

		// Select and change to dev environment popup
		sleep(1900);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

		// Click dev change to dev environment
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Environment changed.");
		
		
		// select login element email 
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();
		System.out.println("eMail icon selected.");

	
		
		// select phone button
		sleep(1900);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.ImageView[2]")).click();
		System.out.println("Phone icon selected.");

	
		// select phone enter text line
		sleep(2900);
		driver.findElement(By.id("com.noke.nokeaccess:id/phone_edit_text")).sendKeys(ph);
		System.out.println("Entetr phone."+ ph + "  ..!" );
	
		// enter password
		sleep(3000);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		sleep(2000);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Show password\"]")).click();
		System.out.println("login..!" );

		// skip tutorial 
		skipTutorial();

		// Select and set location popup
		sleep(4000);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4000);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(4500);

		// Change to store Noke Office Test.
		sleep(4500);
		driver.findElement(By.id("com.noke.nokeaccess:id/switch_site_button")).click();
		System.out.println("Select dropdown to change test site.. !");

		// Enter company store - change to Noke Office test
		sleep(4500);
		MobileElement enternokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView\r\n"));
		enternokeofficetest.click();
		System.out.println("Select Noke Office Test.. !");
		sleep(4500);
		System.out.println("Changed to Noke Office Test.. !");

		
		// Enter Select refresh page
		sleep(6000);
		MobileElement refreshpage = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageButton\r\n"));
		refreshpage.click();
		System.out.println("Select refresh from home page.. !");
		sleep(2000);
			
		
		
		sleep(4000);
		
		// Exit to home page
		homepagetoexit();
				

		
		// Mobile app logout from lower menu.
		sleep(2000);
		System.out.println("End DefaultMobilePhoneLoginPageRefreshTests.");
	
	
	}
}
