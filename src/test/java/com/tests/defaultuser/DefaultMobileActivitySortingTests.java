package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

public class DefaultMobileActivitySortingTests extends DefaultBaseClass {

	@Test
	public void defaultMobileActivitySortingTests() {

		logotap();
		sleep(3000);
		System.out.println("Starting DefaultMobileActivitySortingTests.");
		
		
		  // Select and change to dev environment popup
			sleep(1900);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();
			  
			  // Click dev change to dev environment 
			  MobileElement changetodev = driver.findElement(By.id("android:id/button1")); changetodev.click();
			  
			 
			// select login element
			sleep(1900);
			driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();
			
					
			// enter email
			sleep(1900);
			driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
			System.out.println("Enter email as: " +  em );
			
			
			
			// enter password
			sleep(1900);
			driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
			System.out.println("Enter password.");
			
			
			// login arrow
			driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
			
			
			
			// Skip tutorial 
			sleep(4900);
			driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
			System.out.println("Skip tutorial.");
			
			// Select and set location popup
			sleep(4900);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")).click();
			System.out.println("Select Ok accept location popup.");
		
			// Select Allow access
			sleep(4900);
			driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
			System.out.println("Allow access to application.. !");
			

			
			
			// Change to Noke Office Test
			changetoNokeOfficeTest();
			
			
			// Select Activity
			sleep(4900);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")).click();
			System.out.println("Select Activity column.. !");
			
			
			// Select Activity - Units column
			sleep(4900);
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView")).click();
			System.out.println("Select Units column.. !");
			
			
			// Select Activity - Share column
			sleep(4900);
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")).click();
			System.out.println("Select Share column.. !");
			
			
			
			// Exit to home page
			homepagetoexit();
					
		
			System.out.println("End DefaultMobileActivitySortingTests.");
		
	  
	
	}

	
	}


