package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class DefaultMobileSyncFobTests extends DefaultBaseClass {

	@Test
	public void defaultMobileSyncFobTests() {

		
		logotap();
		sleep(2000);
		System.out.println("Starting DefaultMobileSyncFobTests");
		
		// Select and change to dev environment popup
		sleep(2000);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();

		// select login element
		sleep(1800);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();

		// enter email
		sleep(1800);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(500);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		System.out.println("login..!");

		// Skip tutorial
		sleep(5100);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(5100);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(5100);
		MobileElement allowaccesslocation = driver
				.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site default manager  Home Page..!");
		sleep(5100);

		// Access exit page menu
		sleep(5100);
		MobileElement selectmenusys = driver
				.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView"));
		selectmenusys.click();
		System.out.println("Select bottom Menu To Exit page .. !");

		// Access exit page menu select Sync Fob // //
		sleep(5100);
		MobileElement selectsyncfob = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[4]/android.widget.ImageView"));
		selectsyncfob.click();
		System.out.println("Select Sync Fob .. !");
		sleep(6000);
		driver.navigate().back();

		// Mobile app logout from lower menu.
		sleep(2000);
		System.out.println("End DefaultMobileSyncFobTests");


	}
}
