package com.tests.defaultuser;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;

public class DefaultBaseClass {

	static int tc = 13;
	static String em = "default@noke.com";
	static String pw = "password";
	static String ph = "8018375908";
	static String shrph = "8018889999";
	static String pin = "4545";
	
	// Samsung setup.......!
	static String si = "Noke Office Test";
	static String ip = "10.2.10.184:5555"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "SMG965U"; // device name
	static String uid = "3431313949413098"; // UDID device id
	static String pn = "ANDROID";
	static String testloc = "Noke Office Test";
	static String cmdsvr = "C:\\appium -p 4766";
	static AppiumDriver<MobileElement> driver;

	// special parameters
	static String sunit = "3013";
	static String cmdwake = "adb -s " + uid + " shell input keyevent 26";
	static String cmdsleep = "adb -s " + uid + " shell input keyevent 26";
	static String cmdUnlock = "adb -s " + uid + " shell input keyevent 82";
	static String prt = "4766";


	
	// Setup starts server , setsup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
		Runtime.getRuntime().exec("taskkill /F /IM node.exe");
		System.out.println("Refreshing server..: " );
		sleep(2199);
		
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		System.out.println("Waking device up..: " + "Name: "+ dn + ", " + "UID: " + uid );
		sleep(2199);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 82");
		System.out.println("Unlocking device..: " );
		sleep(2199);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input text 4545");
		System.out.println("Entering PIN..: " + pin);
		sleep(2199);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 66");
		System.out.println("Unlocking device with pin..: " );
		sleep(2199);
	
		 {		
			 
		 		CommandLine cmd = new CommandLine("C:\\Program Files\\nodejs\\node.exe");
		 		System.out.println("Command line entry: C:\\Program Files\\nodejs\\node.exe");
			    sleep(1999);
			    cmd.addArgument("C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    System.out.println("Command line entry: C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    sleep(1999);
			    cmd.addArgument("--address");
			    System.out.println("Command line : --address ");
			    sleep(1999);
			    cmd.addArgument("127.0.0.1");
			    System.out.println("Command line entry: 127.0.0.1");
			    sleep(1999);
			    cmd.addArgument("--port");
			    System.out.println("Command line entry: --port ");
			    sleep(1999);
			    cmd.addArgument(prt);
			    System.out.println("Command line entry: " + prt );
			    sleep(1999);
			    cmd.addArgument("--log-level" ,false);
			    cmd.addArgument("error");
	
			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(10999);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			}
		

		sleep(3999);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability("appActivity", "com.noke.storagesmartentry.ui.login.LoginActivity");
			

			URL url = new URL("http://127.0.0.1:4766/wd/hub");

						
			driver = new AppiumDriver<MobileElement>(url, caps);
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
		}
		//tap on logo to change testing environment.
		logotap();
		sleep(3000);
		
		
		
		// Basic email login
		emailLoginBasic();

		
		// Biometrics approval no/yes
		sleep(3000);
		biometricsApproval();

	}


	
	
	
	@AfterTest
	public void teardown() throws IOException {
		 
		driver.runAppInBackground(Duration.ofSeconds(0));
		System.out.println("Exit test..!");
		// Mobile app logout from lower menu.
		System.out.println("App reset .. !");
		driver.resetApp();
		sleep(2900);
		driver.closeApp();
		sleep(2900);
		driver.quit();
		sleep(2900);

		Runtime.getRuntime().exec(cmdsleep);
		System.out.println("Android Asleep .. !");
		sleep(1999);

		{
	}

{
			Runtime runtime = Runtime.getRuntime();
			try {
				
				sleep(3500);
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				sleep(1000);
				System.out.println("===============================================");
				sleep(1999);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	
	
	
	public void logotap() {
		sleep(2000);
		for (int i = 0; i < tc; i++) {
			// click the button
			driver.findElement(By.id("com.noke.nokeaccess:id/main_logo")).click();
			
			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
				
				
	}
	
	
	
	
	public void changetoNokeOfficeTest() {
		// Change to store Noke Office Test.
		sleep(4500);
		MobileElement nokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView"));
		nokeofficetest.click();
		System.out.println("Select dropdown to change test site.. !");

		// Enter company store - change to Noke Office test
		sleep(4500);
		MobileElement enternokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView"));
		enternokeofficetest.click();
		System.out.println("Select Noke Office Test.. !");
		sleep(2000);
		System.out.println("Changed to Noke Office Test.. !");
	}
	
	public void homepagetoexit() {
		// Go to home page to exit 
		sleep(3900);
		MobileElement selecthome = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView\r\n"));
		selecthome.click();
		System.out.println("Home page .. !");
		sleep(3900);
	}
	
	
	
	public void skipTutorial() {
		// Skip tutorial
		sleep(4800);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");
	}
	
	
//	public void logotap() {
//		sleep(2000);
//		for (int i = 0; i < tc; i++) {
//			// click the button
//			MobileElement logochangetodev = driver.findElement(By.id("com.noke.nokeaccess:id/main_logo"));
//			logochangetodev.click();
//			// wait 1/10 seconds
//
//			// check that data is being generated correctly
//		} 		System.out.println("Selecting Logo to change to testing dev environment.");
//	}
	
	
	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
//  workking biometric by itself w no if statement 	
//  do not save biometrics - just verifies message.	Note: Enable acts as if the oppisite.
	public void biometricsApproval() {
	//	do not save biometrics - just verifies message.	
	sleep(5999);
	if(!driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).isEnabled()) {
		System.out.println("No  biometrics Settings available..!");
		sleep(4999);		
	} else
	  {
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
		sleep(4999);
		System.out.println("Do not save biometrics settings..!");
	  }
	}

public void emailLoginBasic() {
	// Select and change to dev environment popup
	sleep(1900);
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

	// Click dev change to dev environment
	driver.findElement(By.id("android:id/button1")).click();

	// select login element
	sleep(1900);
	driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();

	// enter email
	sleep(1900);
	driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
	System.out.println("Enter email as: " + em);

	// enter password
	sleep(1900);
	driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
	System.out.println("Enter password.");

	// login arrow
	sleep(1900);
	driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
	System.out.println("login..!");
}
public void changeTestLocation() {
	// Change test location to Noke Office test
	sleep(3999);
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
	System.out.println("Select dropdown to change test location.");
	// Select spyglass 
	sleep(3999);
	driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
	System.out.println("Select spyglass.");
	// Enter location 
	sleep(3999);
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(testloc);
	System.out.println("Enter test location: " + testloc + " ..!" );
	// Select test location
	sleep(5999);
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
	System.out.println("Select  test location.");
}

}

