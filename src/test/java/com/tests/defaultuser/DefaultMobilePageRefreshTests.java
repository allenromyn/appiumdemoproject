package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class DefaultMobilePageRefreshTests extends DefaultBaseClass {

	@Test
	public void defaultMobilePageRefreshTests() {

		
		logotap();
		sleep(0);
		System.out.println("Starting DefaultMobilePageRefreshTests.");

		
		// Select and change to dev environment popup
		sleep(2000);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();

		// select login element
		sleep(1400);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();

		// enter email
		sleep(1400);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(500);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		System.out.println("login..!");

		// skip tutorial 
		skipTutorial();

		// Select and set location popup
		sleep(4500);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4500);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(4500);

		// Change to store Noke Office Test.
		sleep(4500);
		MobileElement nokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView"));
		nokeofficetest.click();
		System.out.println("Select dropdown to change test site.. !");

		// Enter company store - change to Noke Office test
		sleep(4500);
		MobileElement enternokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView"));
		enternokeofficetest.click();
		System.out.println("Select Noke Office Test.. !");
		sleep(2000);
		System.out.println("Changed to Noke Office Test.. !");

		
		
		
		
		
		
		
		
		// Enter Select refresh page
		sleep(4500);
		MobileElement refreshpage = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageButton"));
		refreshpage.click();
		System.out.println("Select refresh from home page.. !");
		sleep(2000);
		
			
		
		// Exit to home page
		homepagetoexit();
	
			
		// Mobile app logout from lower menu.
		sleep(2000);
		System.out.println("End DefaultMobilePageRefreshTests.");

	
	
	}
}
