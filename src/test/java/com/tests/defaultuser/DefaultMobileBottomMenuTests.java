package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class DefaultMobileBottomMenuTests extends DefaultBaseClass {

	@Test
	public void defaultMobileBottomMenuTests() {

		logotap();
		sleep(2000);
		System.out.println("Starting DefaultMobileBottomMenuTests.");
		
		  // Select and change to dev environment popup
		sleep(1800);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();
		  
		  // Click dev change to dev environment 
		driver.findElement(By.id("android:id/button1")).click();
		  
		 
		// select login element
		sleep(1800);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();
		
				
		// enter email
		sleep(1800);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " +  em );
		
		
		
		// enter password
		sleep(1800);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		
		
		
		// Skip tutorial 
		sleep(4900);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");
		
		// Select and set location popup
		sleep(4900);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");
	
		// Select Allow access
		sleep(4900);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");
		

		
		
		// Change to Noke Office Test
		changetoNokeOfficeTest();
		
		
		// Select Units tab
		sleep(6000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Selected Units tab bottom menu..!");
		
		
		// Select Users tab.
		sleep(6000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView")).click();
		System.out.println("Selected Users tab bottom menu..!");
		
		
		// Select Activity tab.
		sleep(6000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")).click();
		System.out.println("Selected Activity tab bottom menu..!");
			
		
		
		// Select Home tab.
		sleep(6000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView")).click();
		System.out.println("Selected Home tab bottom menu..!");
		
		// Exit to home page
		sleep(3000);
		homepagetoexit();

		
		// Mobile app logout from lower menu.
		System.out.println("End DefaultMobileBottomMenuTests.");
	
		

	}
}


