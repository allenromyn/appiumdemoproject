package com.tests.defaultuser;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class DefaultMobileShareUnitTests extends DefaultBaseClass {
	static String curmod = "DefaultMobileShareUnitTests";
	
	
	@Test
	public void defaultMobileShareUnitTests() {
		
		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);
	

		// Skip tutorial
		sleep(4999);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(4999);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");
		// Select Allow access
		sleep(4999);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(4999);

		
		// Select Unit menu from bottom
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from menu.. !");
		sleep(2999);
		
		
		// Select share unit plus sign upper right hand corner
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"add button\"]")).click();
		System.out.println("Select plus sign .. !");
		sleep(2999);
		
		// Select share Unit
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView")).click();
		System.out.println("Select share unit .. !");
		sleep(2999);
		
		// Eneter phone number to share with
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.EditText")).sendKeys(shrph);
		System.out.println("Enter number .. !");
		sleep(2999);
		
	
		
		// Select arrow to select unit
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView")).click();
		System.out.println("Select arrow .. !");
		sleep(2999);
	
		
		
		// Select first unit in list
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
		System.out.println("Select Unit to share .. !");
		sleep(4900);
		
		
		
		
		// Select yes to share.
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		System.out.println("Select Yes to share, Will open homepage .. !");
		driver.navigate().back();

		// view unit after share 
		sleep(3000);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Units from menu .. !");
		sleep(5500);
		
		
		
		// Select unit
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Select Unit to view share .. !");
		sleep(7999);
		
		
		// Select users to view 
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Users to view.. !");
		sleep(5500);
		driver.navigate().back();
		driver.navigate().back();
		
		
		
	
		// Mobile app logout from lower menu.
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*********************************");
		sleep(4999);
	

	}

}
