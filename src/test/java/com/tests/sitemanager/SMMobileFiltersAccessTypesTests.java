package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;






public class SMMobileFiltersAccessTypesTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileFiltersAccessTypesTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileFiltersAccessTypesTests"; 
	
	@Test
	public void sMMobileFiltersAccessTypesTests() {
		

		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	
	

		// select units menu from menu bar
		sleep(999);
		WebDriverWait waitAccessFilters = new WebDriverWait(driver, 20);
		waitAccessFilters.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Menu selected to access Filters  .. !");
		sleep(2999);

		
		// Select Filter tab
		WebDriverWait waitFiltersTab = new WebDriverWait(driver, 20);
		waitFiltersTab.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		MobileElement selectfilterstab = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n"));
		selectfilterstab.click();
		System.out.println("Select Filters tab .. !");
		sleep(2999);

		// Select from Access Type section
		WebDriverWait waitSelectRentable = new WebDriverWait(driver, 20);
		waitSelectRentable.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]\r\n")).click();
		System.out.println("Select Rentable  .. !");
		sleep(2999);

		// X out to previous page
		exitFilterPage();
		System.out.println("Displays ..!");
		sleep(999);
 			
		
		// Select filter tab
		selectfilterstab.click();
		
		// Select Clear filter
		clearFilter();
		
		// Select Access Type Site 
		WebDriverWait waitSelectSite = new WebDriverWait(driver, 20);
		waitSelectSite.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]\r\n")).click();
		System.out.println("Select Site.. !");
		sleep(4999);

		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Site Units..!");
		sleep(4999);
 
		// Select filter tab
		selectfilterstab.click();

		// Select Clear filter
		clearFilter();

		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Units..!");
		sleep(999);
 			
		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
			
	}
	
}

