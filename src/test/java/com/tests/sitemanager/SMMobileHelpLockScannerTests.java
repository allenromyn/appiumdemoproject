package com.tests.sitemanager;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/*import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
public class SMMobileHelpLockScannerTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsHelpSupportLinkTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileHelpLockScannerTests";

	@Test
	public void sMMobileHelpLockScannerTests() {
	
		
		System.out.println("Starting :" + curmod + " test.");
		
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();



		// select lower menu.
		WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
		waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Help & Support  .. !");
		sleep(999);
		
		// Access Lock Scanner , view info
		WebDriverWait waitSyncFobForTenant = new WebDriverWait(driver, 20);
		waitSyncFobForTenant.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Lock Scanner, view current info.. !");
	    sleep(2499);
		driver.navigate().back();


		
		
		// Exit to home page
		//homepagetoexit();
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");
	
	
		}


	
	}
		



	



