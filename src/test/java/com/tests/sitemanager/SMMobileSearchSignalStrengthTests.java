package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileSearchSignalStrengthTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSearchSignalStrengthTests extends SMSamsungGalaxyG9BaseClass {
	static String sunit = "3013";
	static String curmod = "SMMobileSearchSignalStrengthTests";
	
	
	
	
	@Test
	public void sMMobileSearchSignalStrengthTests() {

		
	 

		System.out.println("Starting :" + curmod + " test.");
				
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();
		
	
			
			// Select Units page
			WebDriverWait waitSelectUnitPage = new WebDriverWait(driver, 20);
			waitSelectUnitPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
			System.out.println("Select Unit page.. !");
			sleep(2999);
			
			// Select seatch spy glass
			WebDriverWait waitSelectSpyGlass = new WebDriverWait(driver, 20);
			waitSelectSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")));
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
			System.out.println("Select spy glass search.. !");
			sleep(2999);
			
			// Select Enter searcj criteria 
			WebDriverWait waitSearchForUnit = new WebDriverWait(driver, 20);
			waitSearchForUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
			System.out.println("Enter unit to search for.. !");
			sleep(2999);
			
			// Select Enter searcj criteria 
			WebDriverWait waitViewUnitInfo = new WebDriverWait(driver, 20);
			waitViewUnitInfo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
			System.out.println("View unit info.. !");
			sleep(1999);
			driver.navigate().back();
			sleep(1999);
			driver.navigate().back();
			
	
			// Exit to home page
			// Select home from lower menu 
			System.out.println("Ending :" + curmod + " test.");
			System.out.println("***************************************");
			sleep(2999);
			// Select More - lower right corner - Exit swipe up - slide 
			selectMoreLowerRight();
			// Swipe up to locate logout link.
			swipeBottomToTop();
			// Select logout
			logoutWithPopup();				
			System.out.println("****************************");
		}
		
	}

