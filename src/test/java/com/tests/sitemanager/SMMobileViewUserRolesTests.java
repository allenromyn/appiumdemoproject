package com.tests.sitemanager;



import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;




//public class SMMobileViewUserRolesTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileViewUserRolesTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileViewUserRolesTests";
	static String pw = "password";	
	
	@Test
	public void sMMobileViewUserRolesTests() {
	
		System.out.println("Starting :" + curmod + " test.");

	
		// skip tutorial
		skipTutorial();
	
		//location and access popups
		locationAccessDevice();
		
		// Change test location
		changeTestLocation();

		
		// Access exit page 
		// select lover menu.
		WebDriverWait waitSelectExitPage = new WebDriverWait(driver, 20);
		waitSelectExitPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Account .. !");
		sleep(3999);
		
		// Select Account to view User Roles 
		WebDriverWait waitSelectAccountView = new WebDriverWait(driver, 20);
		waitSelectAccountView.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Account to view User Roles.. !");
		sleep(3999);
		// Select Account - back
		driver.navigate().back();
		


		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}

	
}


	
 
