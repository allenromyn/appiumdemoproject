package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
public class SMMobileExitPageLinksTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	String curmod = "SMMobileExitPageLinksTests";

	
	@Test
	public void sMMobileExitPageLinksTests() throws Exception {

		System.out.println("Starting :" + curmod + " test.");
		System.out.println("******************************");
		
		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();
		// Select Bottom menu to 
		selectMoreLowerRight();
		
				// Select About - back
				WebDriverWait waitSelectAboutView = new WebDriverWait(driver, 20);
				waitSelectAboutView.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView\r\n")).click();
				System.out.println("Select About and view.. !");
				sleep(2999);
				goBack();
		
				
				
				
				// Select Access Codes - back
				WebDriverWait waitSelectAccesscodes = new WebDriverWait(driver, 20);
				waitSelectAccesscodes.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Access Codes and view.. !");
				sleep(2999);
				goBack();
		
		

				// Select Account - back
				WebDriverWait waitSelectAccountView = new WebDriverWait(driver, 20);
				waitSelectAccountView.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Account and view.. !");
				sleep(2999);
				goBack();
		
		
				// Select Add Gateway - back 
				WebDriverWait waitSelectAddGateway = new WebDriverWait(driver, 20);
				waitSelectAddGateway.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[4]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[4]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Add Gateway and view.. !");
				sleep(2999);
				// Select Allow  go back to previous page
				System.out.println("Select Allow.. !");
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")).click();
				sleep(2999);
				goBack();
				
		
				// Select Demo Mode
				WebDriverWait waitSelectDemoMode = new WebDriverWait(driver, 20);
				waitSelectDemoMode.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Demo Mode and view.. !");
				sleep(2999);
				goBack();
		
				// Select Device Scanner
				WebDriverWait waitSelectDeviceScanner = new WebDriverWait(driver, 20);
				waitSelectDeviceScanner.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Device Scanner and view.. !");
				sleep(2999);
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button\r\n")).click();
				System.out.println("Select Verify you have the correct unit - OK .. !");
				sleep(2999);
				goBack();
		
				// Select Feeback
				WebDriverWait waitSelectFeedback = new WebDriverWait(driver, 20);
				waitSelectFeedback.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Feedback and view.. !");
				sleep(2999);
				goBack();
		
				// Select Fob lookup
				WebDriverWait waitSelectFobLookup = new WebDriverWait(driver, 20);
				waitSelectFobLookup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Fob Lookup and view.. !");
				sleep(2999);
				goBack();
		
				// Select Gateways
				WebDriverWait waitSelectGateways = new WebDriverWait(driver, 20);
				waitSelectGateways.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Gateways and view.. !");
				sleep(2999);
				goBack();
		
				// Select Help & Support
				WebDriverWait waitSelectHelpandSupport = new WebDriverWait(driver, 20);
				waitSelectHelpandSupport.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Help and Support and view.. !");
				sleep(2999);
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[6]\r\n")).click();
				sleep(2999);
				System.out.println("Select Cancel.. !");
			
		
				// Select Installer Tools 
				WebDriverWait waitSelectInstallerTools = new WebDriverWait(driver, 20);
				waitSelectInstallerTools.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Installer Tools and view.. !");
				sleep(2999);
				goBack();
		
				// Select Mesh Network Status 
				WebDriverWait waitSelectNetworkStatus = new WebDriverWait(driver, 20);
				waitSelectNetworkStatus.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Mesh-Network Status and view.. !");
				sleep(2999);
				goBack();
		
				// Select Quick Start Guide
				WebDriverWait waitSelectQuickStartGuide = new WebDriverWait(driver, 20);
				waitSelectQuickStartGuide.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[13]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[13]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Quick Start Guide and view.. !");
				sleep(2999);
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
				System.out.println("Select Cancel, return to previous page.. !");
				sleep(2999);
		
				
				// Swipe up to locate logout link.
				swipeBottomToTop();
				
				
				// Select Settings 
				WebDriverWait waitSelectSettings = new WebDriverWait(driver, 20);
				waitSelectSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Settings  and view.. !");
				sleep(2999);
				goBack();
		
				
				// Select Sync Fob for Tenants
				WebDriverWait waitSelectSyncFobforTenant = new WebDriverWait(driver, 20);
				waitSelectSyncFobforTenant.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Sync Fob for Tenant and view.. !");
				sleep(2999);
				goBack();
		
				// Select Tutorials 
				WebDriverWait waitSelectTutorials = new WebDriverWait(driver, 20);
				waitSelectTutorials.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")).click();
				System.out.println("Select Tutorials and view.. !");
				sleep(2999);
				goBack();
		
				// Select View end user license agreement 
				WebDriverWait waitSelectEndLicense = new WebDriverWait(driver, 20);
				waitSelectEndLicense.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[13]/android.widget.ImageView\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[13]/android.widget.ImageView\r\n")).click();
				System.out.println("Select View End User license agreement .. !");
				sleep(2999);
				// go back one page
				goBack();
		
		


		
		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
			
	}





















	public void goBack() {
		driver.navigate().back();
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void locationAccessOK() {
		// Select and set location popup
		sleep(4999);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4999);
		MobileElement allowaccesslocation = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");
	}
}
