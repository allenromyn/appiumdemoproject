package com.tests.sitemanager;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

public class SMMobilePageRefreshTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobilePageRefreshTests";
	@Test
	public void sMMobilePageRefreshTests() {


		System.out.println("Starting :" + curmod + " test.");
		System.out.println("****************************");
		sleep(1699);
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();
		
		// Change test location
		changeTestLocation();

		
		
		// Enter Select refresh page
		sleep(2999);
		WebDriverWait waitSelectRefresh = new WebDriverWait(driver, 20);
		waitSelectRefresh.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageButton")));
		MobileElement refreshpage = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageButton"));
		refreshpage.click();
		System.out.println("Select refresh from home page.. !");
		sleep(4999);
		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
		

		
			
	}
	
}

