package com.tests.sitemanager;



import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
public class SMSamsungGalaxyG9BaseNewLoginClass {

	static int tc = 13;
	// Login variables 
	static String em = "allen.romyn+qa@noke.com";
	// static String em = "qa@noke.com";
	// static String pw = "password";
	static String pw = "password1";
	//static String pw = "password";
	static String si = "Noke Office Test";
	static String ph = "80188814999";
	static String pin = "4545";
//	static String ph = "771793336";
	static String ccode = "+46";
	// Special parms
	static String cmdwake = "shell input keyevent 26";
	static String cmdUnlock = "shell input keyevent 82";
	static String cmdPin = "adb shell input text 4545";
	static String cmdPinwake = "adb shell input keyevent 66";
	static String cmdsleep = "adb shell input keyevent 26";
	static String testloc = "Office Overstock Storage";
	static String endtask = "taskkill /F /IM node.exe";
	//static String testloc = "Noke Office Test";
	static String cmdsvr = "C:\\appium -p 4766";
	static String prt = "4766";
	
	// Samsung Galaxy S9
	static String ip = "10.2.10.184:5555"; // IP address
	static String pv = "10.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "SMG965U"; // device name
	static String uid = "3431313949413098"; // UDID device id
	static String pn = "ANDROID";
	
//	static String bclass = "SMSamsungGalaxyG9BaseClass";
//	static String extmeth = "SMSamsungGalaxyG9BaseClass";
	
	// Samsung Galaxy Note 5
//	static String ip = ""; // IP address
//	static String pv = "7.0.0"; // platform version SAMSUNG -- ANDROID
//	static String dn = "SM-N920T"; // device name - Galzxy Note5
//	static String uid = "0715f7bd0c5d093a"; // UDID device id
//	static String pn = "ANDROID";
	
	// Samsung Galaxy A20 
//	static String ip = ""; // IP address
//	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
//	static String dn = "SM-A207M"; // device name - Galzxy A20
//	static String uid = "R9AMB1PEAGJ"; // UDID device id
//	static String pn = "ANDROID";



	static AppiumDriver<MobileElement> driver;

	// Setup starts server , setup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
		Runtime.getRuntime().exec(endtask);
		System.out.println("Refreshing Appium Server..: " );
		// sleep(9999);
		
		System.out.println("Waking device up..: " + "Name: "+ dn + ", " + "UID: " + uid );
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		sleep(699);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 82");
		System.out.println("Unlocking device..: " );
		sleep(699);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input text 4545");
		System.out.println("Entering PIN..: " + pin);
		sleep(699);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 66");
		System.out.println("Unlocking device with pin..: " );
		sleep(699);

		
		
		{		
					  
		 	System.out.println("Starting Appium server..!"); 
		 	sleep(699);
			 	
		 		CommandLine cmd = new CommandLine("C:\\Program Files\\nodejs\\node.exe");
		 		System.out.println("Command line entry: C:\\Program Files\\nodejs\\node.exe");
			    sleep(699);
			    cmd.addArgument("C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    System.out.println("Command line entry: C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    sleep(699);
			    cmd.addArgument("--address");
			    System.out.println("Command line entry: --address ");
			    sleep(699);
			    cmd.addArgument("127.0.0.1");
			    System.out.println("Command line entry: 127.0.0.1");
			    sleep(699);
			    cmd.addArgument("--port");
			    System.out.println("Command line entry: --port ");
			    sleep(699);
			    cmd.addArgument(prt);
			    System.out.println("Command line entry: " + prt );
			    sleep(699);
			    cmd.addArgument("--log-level" ,false);
			    cmd.addArgument("error");
			    System.out.println("Loading appium server commands.");
			    // sleep(899);
			    
	
			    
			    
			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(7999);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			    
		 
		 
		sleep(2999);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			sleep(1699);
//			caps.setCapability("appPackage", "com.noke.nokeaccess");
//			caps.setCapability("appActivity","com.noke.storagesmartentry.ui.login.LoginActivity");
			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability( "appActivity", "com.noke.storagesmartentry.ui.login.LoginValidateActivity");


			URL url = new URL("http://127.0.0.1:4766/wd/hub");
			driver = new AppiumDriver<MobileElement>(url, caps);
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
	
		}
		
		//tap on logo to change testing environment.
		// logotap();
		//tap on logo to change testing environment.
		logoTapNew();
		// Set Environment to DEV
		sleep(699);
		setEnvironmentToDEV();	
		// Basic email login
		// new login flow. Located in Base class for new login flow and update.
		// newLoginFlowAndroid();
		sleep(1699);
		newLoginFlowAndroidNoChange();
		//Biometrics approval no/yes
		sleep(1999);
		biometricsApproval();
		
		}
	
	}









	

	
	// logout , stop servers.
	@AfterTest
	public void teardown() throws IOException {
			
		  // Mobile app logout from lower  menu. // 
		  System.out.println("Reset application..!");
		  driver.resetApp(); 
		  sleep(999); 
		  driver.closeApp(); // 
		  sleep(999); //
		  driver.quit();
		  
		
		  Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		  System.out.println("Android Asleep .. !"); 
		  sleep(999);
		{
					}
		{
			Runtime runtime = Runtime.getRuntime();
			try {
				
				
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("=======================================================");
				sleep(2999);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	// Change to Noke Office Teat
	// public void changenokeofficetest() {
	
		
		
		
	
	
	
	public void logotap() {
		sleep(299);
		for (int i = 0; i < tc; i++) {
//			System.out.println("Tap Logo." + i);
			// click the button
			WebDriverWait waitLogoTap = new WebDriverWait(driver, 20);
			waitLogoTap.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.noke.nokeaccess:id/main_logo")));
			driver.findElement(By.id("com.noke.nokeaccess:id/main_logo")).click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
	}
	

	
	
	
	
	
	
	
	public void homepagetoexit() {
		String curmod = "";
		// Go to exit page to logout
		WebDriverWait waitHomeExit = new WebDriverWait(driver, 20);
		waitHomeExit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Open Exit page.. !");
		sleep(999);
		// Select Logout
		WebDriverWait waitLogout = new WebDriverWait(driver, 20);
		waitLogout.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[13]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[14]\r\n")).click();
		System.out.println("Select Logout.. !");
		// Select logut popup.
		sleep(1299);
		WebDriverWait waitLogoutPopup = new WebDriverWait(driver, 20);
		waitLogoutPopup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		System.out.println("Select Yes..! Logout Popup.. !");
		
	
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("****************************");

	}


	
	
	public void logoutExitComplete() {
		// Logout
		WebDriverWait waitLogOut = new WebDriverWait(driver, 20);
		waitLogOut.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Logout..!");
		waitLogOut.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		System.out.println("Logout Popup..!");
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	public void skipTutorial() {
		// Skip tutorial
		sleep(999);
		WebDriverWait waitSkipTutorial = new WebDriverWait(driver, 20);
		waitSkipTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Skip tutorial.");
	}

	
	
	
	public void changetoDevEnv() {
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		// Click dev change to dev environment 
		driver.findElement(By.id("android:id/button1")).click();
		
	}		
	
	
//  workking biometric by itself w no if statement 	
//  do not save biometrics - just verifies message.	Note: Enable acts as if the oppisite.
	public void biometricsApproval() {
	if (driver.findElements(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).size() > 0) {
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
		System.out.println("Biometrics Settings available, Do not save settings..!");
    } else 	{
    	System.out.println("Biometrics settings not available..!");

    }
	
	}
	
	
	public void emailLoginBasic() {

		// Click dev change to dev environment
		WebDriverWait waitEnvPopupSelect = new WebDriverWait(driver, 20);
		waitEnvPopupSelect.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[3]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[3]\r\n")).click();
		System.out.println("Select logo, tap for environment");

		// select login element change to email
		sleep(999);
		WebDriverWait waitLoginElement = new WebDriverWait(driver, 20);
		waitLoginElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")).click();
		// enter email
		// driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		WebDriverWait waitEmail = new WebDriverWait(driver, 20);
		waitEmail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(em);
		System.out.println("Enter email as: " + em);
		// enter password
		// driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		WebDriverWait waitPassword = new WebDriverWait(driver, 20);
		waitPassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")).sendKeys(pw);
		System.out.println("Enter password.");
		// login arrow
		WebDriverWait waitLoginArrow = new WebDriverWait(driver, 20);
		waitLoginArrow.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		System.out.println("login..!");
	}

	
	
	
	
	
	
	
	
	
	public void changeTestLocation() {
		// Change test location to Noke Office test
		WebDriverWait waitDropdown = new WebDriverWait(driver, 20);
		waitDropdown.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Select dropdown to change test location.");
		sleep(299);
		// Select spyglass 
		WebDriverWait waitImage = new WebDriverWait(driver, 20);
		waitImage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")));
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
		System.out.println("");
		sleep(299);
		// Enter location 
		WebDriverWait waitTestLocation = new WebDriverWait(driver, 20);
		waitTestLocation.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(testloc);
		sleep(3199);
		System.out.println("Enter test location: " + testloc + " ..!" );
		
		// Select test location
		WebDriverWait waitTestLocationMain = new WebDriverWait(driver, 20);
		waitTestLocationMain.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select  test location.");
		sleep(2999);
	}


	
	
	
	public void changeTestSite() {
		// Select change test site - Major test site change .
		sleep(999);
		WebDriverWait waitChangeTestSite = new WebDriverWait(driver, 20);
		waitChangeTestSite.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Select dropdown to change test site.. !");
		// Select change test site spyglass
		sleep(999);
		WebDriverWait waitSelectSpyGlass = new WebDriverWait(driver, 20);
		waitSelectSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")));
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
		System.out.println("Select search-spyglass to change test site.. !");
		// Enter test site name
		sleep(999);
		WebDriverWait waitSearchSpyglass = new WebDriverWait(driver, 20);
		waitSearchSpyglass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(si);
		System.out.println("Select search-spyglass to change test site.. !");
		// Select search value
		sleep(999);
		WebDriverWait waitSelectTestSite = new WebDriverWait(driver, 20);
		waitSelectTestSite.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select test site.. !");
	}
	
	
	
	
	public void clearFilter() {
		// Clear filter method
		sleep(999);
		WebDriverWait waitClearFilters = new WebDriverWait(driver, 20);
		waitClearFilters.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Clear filter.. !");
		
	}

	
	
	
	public void locationAccessDevice() {
		// Select and set location popup
		sleep(1999);
		WebDriverWait waitAcceptLocationPopup = new WebDriverWait(driver, 20);
		waitAcceptLocationPopup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button\r\n")).click();
		System.out.println("Select Ok accept location popup.");
		// Select Allow access
		sleep(1999);
		WebDriverWait waitAllowAccess = new WebDriverWait(driver, 20);
		waitAllowAccess.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")).click();
		// driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");
	}


	
	
	
	
	
	
	
	
	public void selectUnitsFromMenu() {
		// Select Units page
		sleep(999);
		WebDriverWait waitSelectUnit = new WebDriverWait(driver, 20);
		waitSelectUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Units from lower menu.. !");
	}

	
	

	
	
	
	
	
	// New mobile logtap flow
	public void logoTapNew() {
		for (int i = 0; i < tc; i++) {
			// click the button
			WebDriverWait waitLogoTap = new WebDriverWait(driver, 20);
			waitLogoTap.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.noke.nokeaccess:id/main_logo")));
			driver.findElement(By.id("com.noke.nokeaccess:id/main_logo")).click();
			sleep(25); // wait 1/10 seconds
			}  
		System.out.println("Selecting Logo to change to testing dev environment.");
		}
	
	
	// setEnvironmentToDev - mobile android 
	public void setEnvironmentToDEV() {
		WebDriverWait waitSetEnvironment = new WebDriverWait(driver, 20);
		waitSetEnvironment.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]\r\n")).click();
		System.out.println("Set Environment to DEV.");
		sleep(699);
	}

	
	
	
	
	
	
	
	
	public void newLoginFlowAndroid() {
		// New login flow mobile select email tab ( note: default to phone login )
		WebDriverWait waitSelectEmail = new WebDriverWait(driver, 20);
		waitSelectEmail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select User Email.");
		sleep(299);	
		// Enter email from tester/client
		WebDriverWait waitEnterEmail = new WebDriverWait(driver, 20);
		waitEnterEmail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")).clear();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")).sendKeys(em);
		sleep(299);	
		// Select arrow move to enter password
		WebDriverWait waitEnterPassword = new WebDriverWait(driver, 20);
		waitEnterPassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		sleep(299);
		// Enter password ( new been changed to password1
		WebDriverWait waitChangePassword = new WebDriverWait(driver, 20);
		waitChangePassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")).clear();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")).sendKeys(pw);
		sleep(299);
		// Select arrow open app etc
		WebDriverWait waitSelectArrow = new WebDriverWait(driver, 20);
		waitSelectArrow.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
		sleep(299);
	}
	
	
	
	
	
	public void newLoginFlowAndroidNoChange() {
		// New login flow mobile select email tab ( note: default to phone login )
		WebDriverWait waitSelectEmail = new WebDriverWait(driver, 20);
		waitSelectEmail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select User Email.");
		sleep(299);	
		// Enter email from tester/client
		WebDriverWait waitEnterEmail = new WebDriverWait(driver, 20);
		waitEnterEmail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")).clear();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.EditText\r\n")).sendKeys(em);
		sleep(299);	
		// emailLoginBasic();
        loginBar();
//		// Select arrow
//		WebDriverWait waitSelectArrow = new WebDriverWait(driver, 20);
//		waitSelectArrow.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
//		sleep(299);
		// Select to enter password
		WebDriverWait waitEnterPassword = new WebDriverWait(driver, 20);
		waitEnterPassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")).clear();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\r\n")).sendKeys(pw);
		sleep(299);
		
		// emailLoginBasic();
 		loginBarNext();
//		// Select arrow open app etc
//		WebDriverWait waitSelectFinalArrow = new WebDriverWait(driver, 20);
//		waitSelectFinalArrow.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")));
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n")).click();
//		sleep(299);
	}

	
	
	public void loginBar() {
		// login bar to login
		WebDriverWait waitLoginBar = new WebDriverWait(driver, 20);
		waitLoginBar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Select Next - login bar.. !");
	}

	
	public void loginBarNext() {
		// login bar to login
		WebDriverWait waitLoginBarNext = new WebDriverWait(driver, 20);
		waitLoginBarNext.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select login bar - Enter Application.. !");
	}
	
	
	
	public void contactUs() {
		WebDriverWait waitContactUs = new WebDriverWait(driver, 20);
		waitContactUs.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[7]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[7]/android.widget.TextView\r\n")).click();
		System.out.println("Select View Tenant Support- Contact Us.. !");
		sleep(1999);
	}

	
	
	
	
	public void helpAndSupportLink() {
		WebDriverWait waitHelpAndSupport = new WebDriverWait(driver, 20);
		waitHelpAndSupport.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.TextView\r\n")).click();
		System.out.println("Select Help & Support Link  .. !");
		sleep(999);
	}

	
	
	
	
	
	
	public void logoutWithPopup() {
		System.out.println("Select LOGOUT .. !");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[14]\r\n")).click();
		WebDriverWait waitLogoutPopup = new WebDriverWait(driver, 20);
		sleep(2999);
		waitLogoutPopup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		System.out.println("Select Yes..! Logout Popup.. !");
		sleep(2999);
	}


		
	public void selectMoreLowerRight() {
		System.out.println("Select MORE icon  - via lower right side of menu.. !");
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		// Swipe from bottom to top , to bring logout into view 
	}


	// This method is for swiping from bottom to top to locate logout element on last page for exit.
	
	@SuppressWarnings("rawtypes")
	public void swipeBottomToTop() {
		//Swipe from Bottom to Top.
		System.out.println("Swipe from bottom up - view all links ..!");
		TouchAction  action = new TouchAction(driver);	
		Dimension size	=driver.manage().window().getSize();
		int width=size.width;
		int height=size.height;				
		int middleOfX=width/2;
		int startYCoordinate= (int)(height*.7);
		int endYCoordinate= (int)(height*.2);
						
		action.press(PointOption.point(middleOfX, startYCoordinate))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(middleOfX, endYCoordinate)).release().perform();
	}

	
	
	
	
	
	
	
	
	
	
	public static void exitFilterPage() {
		MobileElement selectfiltersxout = driver.findElement(By.id("com.noke.nokeaccess:id/close"));
		selectfiltersxout.click();
		System.out.println("Exit Filter page.. !");
		sleep(4000);
	}
	
	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
