package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileFilterRepeaterOnOffTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileFilterRepeaterOnOffTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileFilterRepeaterOnOffTests";
	static String pw = "password";
	
	
	@Test
	public void sMMobileFilterRepeaterOnOffTests() {
	
		sleep(399);
		System.out.println("Starting :" + curmod + " test.");
		
		// skip tutorial
		skipTutorial();
		
		//location and access popups
		locationAccessDevice();
	
		// Change test location
		changeTestLocation();

		// Select Units page
		sleep(999);
		WebDriverWait waitSelectUnit = new WebDriverWait(driver, 20);
		waitSelectUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Unit page.. !");

		
		// access Filters
		sleep(399);
		WebDriverWait waitFilterPage = new WebDriverWait(driver, 20);
		waitFilterPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")).click();
		System.out.println("Access Filters page.");
		sleep(399);
		
		// Access Sliding door
		sleep(399);
		WebDriverWait waitSlidingDoor = new WebDriverWait(driver, 20);
		waitSlidingDoor.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")).click();
		System.out.println("Select Sliding Door Filter.");
		sleep(399);
	
		// locate Repeater
		sleep(399);
		WebDriverWait waitRepeaterFilter = new WebDriverWait(driver, 20);
		waitRepeaterFilter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]\r\n")).click();
		System.out.println("Select Repeater Filter.");
		sleep(399);
		driver.navigate().back();


		
		// Exit filter page
		sleep(399);
		WebDriverWait waitExitFilterPage = new WebDriverWait(driver, 20);
		waitExitFilterPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Exit Filter page.");

		
		// Exit to home page
		sleep(999);
		//homepagetoexit();
		// Mobile app logout from lower menu.
		sleep(999);
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("****************************");
			
	}
	
}


 
