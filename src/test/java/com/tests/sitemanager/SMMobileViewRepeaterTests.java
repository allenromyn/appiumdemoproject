package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileViewRepeaterTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String sunit = "2012";
	static String curmod = "SMMobileViewRepeaterTests";
	
	
	
	
	@Test
	public void sMMobileSearchforUnitsTests() {

		
		System.out.println("Starting :" + curmod + " test.");
		


		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();

		
		
		// Select Units page
		sleep(6999);
		WebDriverWait waitSelectUnitPage = new WebDriverWait(driver, 20);
		waitSelectUnitPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Unit page.. !");

		// Select search spy glass
		sleep(6999);
		WebDriverWait waitSpyGlassSearch = new WebDriverWait(driver, 20);
		waitSpyGlassSearch.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter search criteria
		sleep(6999);
		WebDriverWait waitEnterUnitSearch = new WebDriverWait(driver, 20);
		waitEnterUnitSearch.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. !");

		// Select Enter search criteria
		sleep(6999);
		WebDriverWait waitRepeaterInfo = new WebDriverWait(driver, 20);
		waitRepeaterInfo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
		System.out.println("View Repeater info.. !");
		sleep(6999);
		driver.navigate().back();
		sleep(6999);
		driver.navigate().back();

		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");

	}

}
