package com.tests.sitemanager;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
// public class SMMobileUnitsSortingbyNameTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileUnitsSortingbyNameTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod  = "SMMobileUnitsSortingbyNameTests";
	
	
	
	
	@Test
	public void sMMobileUnitsSortingbyNameTests() throws IOException {
		
		sleep(999);
		System.out.println("Starting :" + curmod + " test.");
	
		
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();
	

		
	    // Changes test site from home page
		// Modual located in BaseClass -- generic test site change.
		System.out.println("Site Manager Home Page..!");
		// Change test location
		sleep(2999);
		changeTestLocation();

		
		
		// Select Units from lower menu
		sleep(2999);
		WebDriverWait waitUnits = new WebDriverWait(driver, 20);
		waitUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from menu.. !");
		
		
		// Select Units column to sort
		sleep(2999);		
		WebDriverWait waitUnitsColumn = new WebDriverWait(driver, 20);
		waitUnitsColumn.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.TextView")));
		MobileElement selectunitscolumn = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.TextView"));
		selectunitscolumn.click();
		System.out.println("Sort Units Decending .. !");
		sleep(1999);
		selectunitscolumn.click();
		System.out.println("Sort Units Ascending .. !");
		sleep(1999);
		
		
		
			
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}

	
}


	


