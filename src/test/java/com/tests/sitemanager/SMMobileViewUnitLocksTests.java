package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;


public class SMMobileViewUnitLocksTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod  = "SMMobileViewUnitLocksTests";
	
	
	@Test
	public void sMMobileViewUnitLocksTests() {
		

		System.out.println("Starting :" + curmod + " test.");
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	

		
		
		
	

			// Select Units from menu
			sleep(2999);		
			WebDriverWait waitUnitView = new WebDriverWait(driver, 20);
			waitUnitView.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
			System.out.println("Select Unit View.. !");
			
			
			// Sort columns , Sort by Name ascending 
			sleep(2999);
			WebDriverWait waitNameCol = new WebDriverWait(driver, 20);
			waitNameCol.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout")));
			MobileElement coloumbynameasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout"));
			coloumbynameasc.click();
			System.out.println("Sort Name column ascending .. !");
			// Sort by Name descending
			sleep(2999);
			coloumbynameasc.click();
			System.out.println("Sort Name column desending .. !");
					
			
			// Sort by Type ascending 
			sleep(2999);
			WebDriverWait waitTypeCol = new WebDriverWait(driver, 20);
			waitTypeCol.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout")));
			MobileElement coloumbytypeasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout"));
			coloumbytypeasc.click();
			System.out.println("Sort Type column ascending .. !");
			// Sort by Type descending 
			sleep(2999);
			coloumbytypeasc.click();
			System.out.println("Sort Type column desending .. !");
			
			
			// Access lower menu.
			// Select User from lower menu 
			sleep(2999);
			WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
			waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
			System.out.println("Access lower menu, Logout..!");


			
			
			// Exit to home page
			// Select home from lower menu 
			System.out.println("Ending :" + curmod + " test.");
			System.out.println("***************************************");
			sleep(2999);
			// Select More - lower right corner - Exit swipe up - slide 
			selectMoreLowerRight();
			// Swipe up to locate logout link.
			swipeBottomToTop();
			// Select logout
			logoutWithPopup();				
			System.out.println("****************************");
				
		}

		
	}

