package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SMMobileSettingsAdvancedTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsAdvancedTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileSettingsAdvancedTests";
	
	@Test
	public void sMMobileSettingsAdvancedTests() {


		System.out.println("Starting :" + curmod + " test.");
		System.out.println("******************************");

		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	

		// Access lower menu 
		selectMoreLowerRight();
		System.out.println("Select bottom Menu To Exit page .. !");

		

		// Access exit page menu select advanced settings
		sleep(2999);
		// Swipe up to locate logout link.
		swipeBottomToTop();
		WebDriverWait waitAdvancedSettings = new WebDriverWait(driver, 20);
		waitAdvancedSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")).click();
		System.out.println("Select Settings  .. !");
		sleep(999);
		driver.navigate().back();

		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
			
	}
	
}

