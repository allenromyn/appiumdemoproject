package com.tests.sitemanager;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/*import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
public class SMMobileHelpDemoTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsHelpSupportLinkTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileHelpDemoTests";

	@Test
	public void sMMobileHelpDemoTests() {
	
		
		System.out.println("Starting :" + curmod + " test.");
		
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();



		// select lower menu.
		WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
		waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Help & Support  .. !");
		sleep(999);
		
		// Access Demo
		WebDriverWait waitSelectDemo = new WebDriverWait(driver, 20);
		waitSelectDemo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Demo, view current Demo info.. !");
	    sleep(2499);
		driver.navigate().back();


		
		// Exit to home page
		// homepagetoexit();
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");
	
	
		}


	
	}
		



	



