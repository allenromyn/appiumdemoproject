package com.tests.sitemanager;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
// public class SMMobileUnitsSortingbyTypeTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileUnitsSortingbyTypeTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String new_store = "Office Overstock Storage";
	static String curmod  = "SMMobileUnitsSortingbyTypeTests";
	
	@Test
	public void sMMobileUnitsSortingbyTypeTests() throws IOException {
		
		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
	    // Changes test site from home page
		// Modual located in BaseClass -- generic test site change.
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();

		
			
		// Select Units from lower menu
		sleep(999);
		WebDriverWait waitUnits = new WebDriverWait(driver, 20);
		waitUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from menu.. !");
		
		
		// Select Units column to Type 
		sleep(2999);
		WebDriverWait waitUnitTypes = new WebDriverWait(driver, 20);
		waitUnitTypes.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout")));
		MobileElement selectactivitytype = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout"));
		selectactivitytype.click();
		System.out.println("Sort Unit Types Decending .. !");
		sleep(2999);
		selectactivitytype.click();
		System.out.println("Sort Unit Types Ascending .. !");
		
		
		
			
	}

	
}

	


