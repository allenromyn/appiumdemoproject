package com.tests.sitemanager;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileActivitySortingTests extends SMSamsungGalaxyG9BaseNewLoginClass {
// public class SMMobileActivitySortingTests extends SMSamsungGalaxyG9BaseClass {
	String curmod = "SMMobileActivitySortingTests";

	
	@Test
	public void sMMobileActivitySortingTests() throws IOException {
		
		System.out.println("Starting :" + curmod + " test.");
		sleep(2999);



	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
		
		
		
		// Select Activity
		sleep(999);
		WebDriverWait waitActivityColumn = new WebDriverWait(driver, 20);
		waitActivityColumn.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Activity column.. !");

		// Select Activity - Units column
		sleep(999);
		WebDriverWait waitUnitsColumn = new WebDriverWait(driver, 20);
		waitUnitsColumn.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView\r\n")).click();
		System.out.println("Select Units column.. !");

		// Select Activity - Share column
		sleep(999);
		WebDriverWait waitShareColumn = new WebDriverWait(driver, 20);
		waitShareColumn.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")));
		driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")).click();
		System.out.println("Select Share column.. !");

		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
			
	}


}


