package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SMMobileSearchRemoteUnlockUnitTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSearchRemoteUnlockUnitTests extends SMSamsungGalaxyG9BaseClass {
	static String sunit = "1007";
	static String curmod = "SMMobileSearchRemoteUnlockUnitTests"; 
	
	@Test
	public void sMMobileSearchRemoteUnlockUnitTests() {

		
		
		
		// login bar, login page - from baseclass.
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();
		// biometricsApproval << located in base class , this is an if statement.

		// Select Allow access
		sleep(999);		
		WebDriverWait waitAllowAccess = new WebDriverWait(driver, 20);
		waitAllowAccess.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.android.packageinstaller:id/permission_allow_button")));
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		

		
		// Changes test site from home page
		// Modual located in BaseClass -- generic test site change.
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();

		
		
		
		
		// Select Units page
		sleep(2999);
		WebDriverWait waitSelectUnitPage = new WebDriverWait(driver, 20);
		waitSelectUnitPage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Unit page.. !");

		// Select search spy glass
		sleep(2999);
		WebDriverWait waitSelectSpyGlass = new WebDriverWait(driver, 20);
		waitSelectSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter search criteria
		sleep(2999);
		WebDriverWait waitSelectRefresh = new WebDriverWait(driver, 20);
		waitSelectRefresh.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(sunit); 
		System.out.println("Enter unit to search for.. !");

		// Select Enter search criteria
		sleep(2999);
		WebDriverWait waitViewUnit = new WebDriverWait(driver, 20);
		waitViewUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("View unit info.. !");
		sleep(699);
		
		
		// Remote unlock from lock info
		WebDriverWait waitUnlockSpecificUnit = new WebDriverWait(driver, 20);
		waitUnlockSpecificUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.Button\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.Button\r\n")).click();
		System.out.println("Unlock specific unit..!");
		sleep(499);
		driver.navigate().back();
		
		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}
	
}

