package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


//public class SMMobileViewUnitTemperatureTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileViewUnitTemperatureTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String sunit = "1007";
	static String curmod = "SMMobileViewUnitTemperatureTests";

	
	
	
	@Test
	public void sMMobileViewUnitTemperatureTests() {

		System.out.println("Starting :" + curmod + " test.");
		sleep(999);



		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();


	    // Changes test site from home page
		// Modual located in BaseClass -- generic test site change.
		System.out.println("Site Manager Home Page..!");
		// Change test location
		//sleep(5999);
		changeTestLocation();


		// Select Units page
		selectUnitsFromMenu();
		

		// Select seatch spy glass
		sleep(999);
		WebDriverWait waitSpyGlass = new WebDriverWait(driver, 20);
		waitSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter search criteria
		sleep(2999);
		WebDriverWait waitUnitToSearch = new WebDriverWait(driver, 20);
		waitUnitToSearch.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. !");

		// Select Enter search criteria
		sleep(999);
		WebDriverWait waitUnitTemperature = new WebDriverWait(driver, 20);
		waitUnitTemperature.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
		System.out.println("View unit Temperature.. !");
		sleep(3999);
		driver.navigate().back();
		sleep(3999);
		driver.navigate().back();
		

		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}

	
}

	