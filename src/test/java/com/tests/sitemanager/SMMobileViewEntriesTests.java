package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class SMMobileViewEntriesTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileViewEntriesTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileViewEntriesTests";
	// static String pw = "password";	
	
	@Test
	public void sMMobileViewEntriesTests() {
	
		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();

		
		// View Entries 
		sleep(999);
		WebDriverWait waitEntries = new WebDriverWait(driver, 20);
		waitEntries.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout\r\n")).click();
		System.out.println("Select Entries to view .. !");
		
		// View Entries 
		sleep(999);
		WebDriverWait waitViewUnits = new WebDriverWait(driver, 20);
		waitViewUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button\r\n")).click();
		System.out.println("Select View Units .. !");
	
	
		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// homepagetoexit();
			
	}

	
}

