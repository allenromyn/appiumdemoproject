package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileViewGatewayDetailTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	//public class SMMobileViewGatewayDetailTests extends SMSamsungGalaxyG9BaseClass {
	static String sunit = "1005";
	static String curmod = "SMMobileViewGatewayDetailTests";

	
	
	
	@Test
	public void sMMobileViewGatewayDetailTests() {

		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();


		
		// Select exit from menu
		sleep(1999);
		WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
		waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")).click();
		System.out.println("Select from lower menu.. !");

		// Select gateway 
		sleep(2999);
		WebDriverWait waitSelectGateways = new WebDriverWait(driver, 20);
		waitSelectGateways.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.TextView")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.TextView")).click();
		System.out.println("Select Gateway.. !");
		sleep(1999);
		driver.navigate().back();
		
		
			
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");

			
	}

	
}

