package com.tests.sitemanager;

import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
/*import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;

public class SMSamsungGalaxyLMV40BaseClass {

	static int tc = 13;
	// Login variables 
	static String em = "qa@noke.com";
	static String pw = "password";
	static String si = "Noke Office Test";
	static String ph = "8018889999";
	
	// Special parms
	static String cmdwake = "adb shell input keyevent 26";
	static String cmdsleep = "adb shell input keyevent 26";
	static String cmdUnlock = "adb shell input keyevent 82";
	
	// Samsung Galaxy Note 5
	static String ip = "10.2.10.148:5700"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "LM V405"; // device name
	static String uid = "LMV405TAb4ecb5e6"; // UDID device id
	static String pn = "ANDROID";
	static String prt = "4733";
	
	


	static AppiumDriver<MobileElement> driver;

	// Setup starts server , setup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
		
//		Runtime.getRuntime().exec(cmdsvr);
//		sleep(9000);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		System.out.println("Waking device up..!");
		sleep(8900);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 82");
		System.out.println("Unlocking device..!");
		sleep(8900);
	
		{		
	
		  
		 
		 
		 {
			 System.out.println("Starting server..!"); 
			 	sleep(3900);
				 	
				    CommandLine cmd = new CommandLine("C:\\Program Files (x86)\\Appium\\node.exe");
				    System.out.println("Command line entry: C:\\Program Files (x86)\\Appium\\node.exe..!");
				    sleep(3900);
				    cmd.addArgument("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\Appium.js");
				    System.out.println("Command line entry: C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\Appium.js2..!");
				    sleep(3900);
				    cmd.addArgument("--address");
				    System.out.println("Command line : --address ");
				    sleep(3900);
				    cmd.addArgument("127.0.0.1");
				    System.out.println("Command line entry: 127.0.0.1");
				    sleep(3900);
				    cmd.addArgument("--port");
				    System.out.println("Command line entry: --port ");
				    sleep(3900);
				    cmd.addArgument(prt);
				    System.out.println("Command line entry: " + prt );
				    sleep(3900);
				    cmd.addArgument("--log-level" ,false);
				    cmd.addArgument("error");
		
				    
				    
				    
				    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
				    DefaultExecutor executor = new DefaultExecutor();
				    executor.setExitValue(1);
				    try {
				        executor.execute(cmd, handler);
				        Thread.sleep(9900);
				    } catch (IOException | InterruptedException e) {
				        e.printStackTrace();
				    }
		sleep(5900);
	
		
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability("appActivity", "com.noke.storagesmartentry.ui.login.LoginActivity");
			

			URL url = new URL("http://127.0.0.1:4733/wd/hub");

						
			driver = new AppiumDriver<MobileElement>(url, caps);
			
			
			
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
		

	}
		}
		 
	}

}

	
	// logout , stop servers.
	@AfterTest
	public void teardown() throws IOException {
			
		  // Mobile app logout from lower  menu. // 
		  sleep(3000); 
		  System.out.println("Logout .. and reset application..!");
		  System.out.println("Reset App .. !"); //
		  driver.resetApp(); 
		  sleep(3000); 
		  driver.closeApp(); // 
		  sleep(3900); //
		  driver.runAppInBackground(Duration.ofSeconds(0)); // 
		  sleep(3000); //
		  driver.quit();
		  
		
		  Runtime.getRuntime().exec(cmdsleep);
		  System.out.println("Android Asleep .. !"); 
		  sleep(4900);
		{
					}
		{
			Runtime runtime = Runtime.getRuntime();
			try {
				
				sleep(2900);
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("===============================================");
				sleep(5000);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	
	
	
	
	
	
	
	// Change to Noke Office Teat
		public void changenokeofficetest() {
			// Change to store Noke Office Test.
			sleep(4500);
			MobileElement nokeofficetest = driver.findElement(By.id("com.noke.nokeaccess:id/switch_site_button"));
			nokeofficetest.click();
			System.out.println("Change to Noke Office Test.. !");

			// Enter company store - change to Noke Office test
			sleep(4500);
			MobileElement enternokeofficetest = driver.findElement(By.id("com.noke.nokeaccess:id/title"));
			enternokeofficetest.click();
			System.out.println("Select Noke Office Test.. !");
			sleep(2000);
			System.out.println("Changed to Noke Office Test.. !");
		
		}
		
	
	
	
	public void logotap() {
		sleep(2000);
		for (int i = 0; i < tc; i++) {
			// click the button
			MobileElement logochangetodev = driver.findElement(By.id("com.noke.nokeaccess:id/main_logo"));
			logochangetodev.click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
	}
	
	
	public void homepagetoexit() {
		// Go to home page to exit 
		MobileElement selecthome = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView"));
		selecthome.click();
		System.out.println("Home page .. !");
		sleep(4500);
	}

	
	public void skipTutorial() {
		// Skip tutorial 
		sleep(4500);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");
	}	
	
	
	public void changetoDevEnv() {
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		// Click dev change to dev environment 
		driver.findElement(By.id("android:id/button1")).click();
		
	}		
	
	
	
	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
