package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;



//public class SMMobileFiltersEntryTypesTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileFiltersEntryTypesTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileFiltersEntryTypesTests"; 
	
	@Test
	public void sMMobileFiltersEntryTypesTests() {
		

		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	
	
		// Select Units page
		sleep(999);
		WebDriverWait waitSelectUnits = new WebDriverWait(driver, 20);
		waitSelectUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Unit page.. !");

		
		// Select Filter tab
		WebDriverWait waitSelectFiltersTab = new WebDriverWait(driver, 20);
		waitSelectFiltersTab.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n")));
		MobileElement selectfilterstab = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.TextView\r\n"));
		selectfilterstab.click();
		System.out.println("Select Filters tab .. !");
		sleep(999);

		// Select from Entry Type Unit section
		WebDriverWait waitSelectUnitFilter = new WebDriverWait(driver, 20);
		waitSelectUnitFilter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]\r\n")).click();
		System.out.println("Select Unit  .. !");
		sleep(999);

		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Units..!");
		sleep(1999);
 			
		
		// Select filter tab
		selectfilterstab.click();
		
		// Select Clear filter
		clearFilter();
		
		// Select Entry  Type Gate 
		WebDriverWait waitSelectGateFilter = new WebDriverWait(driver, 20);
		waitSelectGateFilter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]\r\n")).click();
		System.out.println("Select Gate.. !");
		sleep(999);

		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Gates..!");
		sleep(1999);
 
		// Select filter tab
		selectfilterstab.click();

		// Select Clear filter
		clearFilter();

		// Select Entry Type Sliding doors 
		WebDriverWait waitSelectSlidingDoorFilter = new WebDriverWait(driver, 20);
		waitSelectSlidingDoorFilter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]\r\n")).click();
		System.out.println("Select Sliding door.. !");
		sleep(999);
		
		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Sliding door..!");
		sleep(999);
 			
		// Select filter tab
		selectfilterstab.click();

		// Select Clear filter
		clearFilter();
	
		// X out to previous page
		exitFilterPage();
		System.out.println("Displays Sliding door..!");
		sleep(1999);
 	
		
		
		// Exit to home page
		sleep(999);
		// homepagetoexit();
		// Mobile app logout from lower menu.
		sleep(999);
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("****************************");
			
	}
	
}

