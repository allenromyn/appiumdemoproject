package com.tests.sitemanager;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/*import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
public class SMMobileHelpInstallerToolsTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsHelpSupportLinkTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileHelpInstallerToolsTests";

	@Test
	public void sMMobileHelpInstallerToolsTests() {
	
		
		System.out.println("Starting :" + curmod + " test.");
		
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();



		// select lower menu.
		WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
		waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Help & Support  .. !");
		sleep(999);
		
		// Access Installer tools 
		WebDriverWait waitSelectInstaller = new WebDriverWait(driver, 20);
		waitSelectInstaller.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Installer Tools.. !");
		sleep(3999);
		
		// Select, explore Gateways. Online
		WebDriverWait waitGatewayOnline = new WebDriverWait(driver, 20);
		waitGatewayOnline.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.TextView\r\n")).click();
		System.out.println("Select Gateways Online.. !");
		sleep(2499);
		driver.navigate().back();


		// Select, explore Gateways. Offline
		WebDriverWait waitGatewayOffline = new WebDriverWait(driver, 20);
		waitGatewayOffline.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.TextView\r\n")).click();
		System.out.println("Select Gateways Offline.. !");
		sleep(2499);
		driver.navigate().back();

		
		// Select, explore Unit Controllers Online
		WebDriverWait waitControllersOnline = new WebDriverWait(driver, 20);
		waitControllersOnline.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.TextView\r\n")).click();
		System.out.println("Select Controllers Online.. !");
		sleep(2499);
		driver.navigate().back();

		// Select, explore Unit Controllers Offline
		WebDriverWait waitControllersOffline = new WebDriverWait(driver, 20);
		waitControllersOffline.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[6]/android.widget.TextView\r\n")).click();
		System.out.println("Select Controllers Offline.. !");
		sleep(2499);
		driver.navigate().back();

		
		// Select, explore Setup
		WebDriverWait waitSetup = new WebDriverWait(driver, 20);
		waitSetup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.TextView\r\n")).click();
		System.out.println("Select Setup.. !");
		sleep(2499);
		driver.navigate().back();

		// Select, explore Needs Setup
		WebDriverWait waitNeedsSetup = new WebDriverWait(driver, 20);
		waitNeedsSetup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.TextView\r\n")).click();
		System.out.println("Select Needs Setup.. !");
		sleep(2499);
		driver.navigate().back();

		
		// Select, explore Lock Tested
		WebDriverWait waitLockTested = new WebDriverWait(driver, 20);
		waitLockTested.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.TextView\r\n")).click();
		System.out.println("Select Lock Tested.. !");
		sleep(2499);
		driver.navigate().back();

		// Select, explore Lock Needs Testing 
		WebDriverWait waitLockNotTested = new WebDriverWait(driver, 20);
		waitLockNotTested.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.TextView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.TextView\r\n")).click();
		System.out.println("Select Lock Needs Testing.. !");
		sleep(2499);
		driver.navigate().back();
		sleep(2499);
		driver.navigate().back();


		
		// Exit to home page
		//homepagetoexit();
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");
	
	
		}


	
	}
		



	



