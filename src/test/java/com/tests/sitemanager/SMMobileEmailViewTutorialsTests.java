package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

//public class SMMobileEmailViewTutorialsTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileEmailViewTutorialsTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileEmailViewTutorialsTests";
	
	
	
	@Test
	public void sMMobileEmailViewTutorialsTests() {
		
		
		sleep(1999);
		System.out.println("Starting :" + curmod + " test.");
	
		
		// View first - second  page of tutorial
		System.out.println("Review first page of Tutorial..!");
		WebDriverWait waitSecondPageTutorial = new WebDriverWait(driver, 20);
		waitSecondPageTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Review second Search tutorial..!");
		sleep(1999);
		// View third page of tutorial
		WebDriverWait waitThirdPageTutorial = new WebDriverWait(driver, 20);
		waitThirdPageTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Review third Units and Entries tutorial..!");
		sleep(1999);
		// View forth page of tutorial
		WebDriverWait waitForthPageTutorial = new WebDriverWait(driver, 20);
		waitForthPageTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Review forth page Add Fobs and Units tutorial..!");
		sleep(1999);
		// View fith  page of tutorial
		WebDriverWait waitFithPageTutorial = new WebDriverWait(driver, 20);
		waitFithPageTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Review Fith  page More Settings  tutorial..!");
		sleep(1999);
		// Done with  tutorial
		WebDriverWait waitLastPageTutorial = new WebDriverWait(driver, 20);
		waitLastPageTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Review Last  page More Settings  tutorial..!");
		sleep(1999);
		

		//location and access popups
		locationAccessDevice();

		// Change test location
		sleep(1699);
		changeTestLocation();


		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}
	
}




