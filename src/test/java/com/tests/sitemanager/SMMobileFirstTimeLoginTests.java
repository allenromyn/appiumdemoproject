package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.testng.annotations.Test;



public class SMMobileFirstTimeLoginTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileFirstTimeLoginTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileFirstTimeLoginTests";
	static String pw = "password";	
	
	@Test
	public void sMMobileFirstTimeLoginTests() {
	
		sleep(2999);
		System.out.println("Starting :" + curmod + " test.");


		
		// First time login 
		
		System.out.println("Open first time login page.");
		sleep(4999);
		driver.findElement(By.xpath("//*[text()='Password']")).sendKeys(pw);
		System.out.println("Enter current password.");
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText\r\n")).sendKeys(pw);
		System.out.println("Confirm current password.");
		sleep(4999);
	
		// Accept - raidio button 
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.CheckBox\r\n")).click();
		System.out.println("Check Consent for install.");
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.LinearLayout[2]/android.widget.CheckBox\r\n")).click();
		System.out.println("Check Consent to receive information.");
		sleep(4999);
		
		
		// Save first time login info
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button\r\n")).click();
		System.out.println("Save first time login info.");
		sleep(4999);
		
		
		
		// Skip tutorial
		sleep(6999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(6999);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(6999);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		
		// Change test location
		changeTestLocation();

		
		
		
		
		
		
	
		sleep(6999);
		System.out.println("Ending :" + curmod + " test.");
		sleep(4999);

	
		
			

		}

	
	}


 
 
