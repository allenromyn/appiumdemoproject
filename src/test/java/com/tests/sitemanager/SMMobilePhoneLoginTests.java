package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.IOException;





public class SMMobilePhoneLoginTests extends SMSamsungGalaxyG9BaseNewLoginClass {	
	static String curmod = "SMMobilePhoneLoginTests";
	String ph = "8015556789";
	
	@Test
	public void sMMobilePhoneLoginTests() throws IOException  {
		
		System.out.println("Starting :" + curmod + " test.");	
		

	
		// Skip tutorial
		WebDriverWait waitSkipTutorial = new WebDriverWait(driver, 20);
		waitSkipTutorial.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Skip tutorial.");

		//location and access popups
		locationAccessDevice();

	
	
		
		sleep(999);
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");

		}
	
	}


