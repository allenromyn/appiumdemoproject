package com.tests.sitemanager;

import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;

public class SMHuaweiBaseClass {

	static int tc = 13;
	static String em = "qa@noke.com";
	static String pw = "password";
	static String si = "Noke Office Test";
	
	// Samsung Huawei 
	static String ip = "10.2.10.139:5555"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "WAS-LX3"; // device name
	static String uid = "H3RDU18319001383"; // UDID device id
	static String pn = "ANDROID";
	
	
	
	

	static AppiumDriver<MobileElement> driver;

	
	
	// Setup starts server , setup Desired Capabilities.
	@BeforeTest
	public void setup() {
		
		 {
			    CommandLine cmd = new CommandLine("C:\\Program Files (x86)\\Appium\\node.exe");
			    cmd.addArgument("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\Appium.js");
			    cmd.addArgument("--address");
			    cmd.addArgument("127.0.0.1");
			    cmd.addArgument("--port");
			    cmd.addArgument("5000");

			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(9000);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			}
	
		sleep(3500);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability("appActivity", "com.noke.storagesmartentry.ui.login.LoginActivity");
			

			URL url = new URL("http://127.0.0.1:4725/wd/hub");

						
			driver = new AppiumDriver<MobileElement>(url, caps);
			
			
			
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
		}

	}


	
	
	
	
	// logout , stop servers.
	@AfterTest
	public void teardown() {
		System.out.println("Exit test..!");
		// Mobile app logout from lower menu.
		System.out.println("Close and reset App .. !");
		driver.resetApp();
		sleep(5000);
		driver.closeApp();
		sleep(5000);
		driver.runAppInBackground(Duration.ofSeconds(0));
		sleep(5000);
		driver.quit();

		{
		}

		{
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("===============================================");
				sleep(3000);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}
	
	
	
	public void logotap() {
		sleep(2000);
		for (int i = 0; i < tc; i++) {
			// click the button
			MobileElement logochangetodev = driver.findElement(By.id("com.noke.nokeaccess:id/main_logo"));
			logochangetodev.click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
	}
	
	

	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
