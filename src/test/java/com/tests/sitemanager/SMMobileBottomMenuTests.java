package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
	

public class SMMobileBottomMenuTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileBottomMenuTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileBottomMenuTests"; 
	
	@Test
	public void sMMobileBottomMenuTests() {
		

		System.out.println("Starting :" + curmod + " test.");
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	
	

			
			
			
			// Select View Units from menu
			WebDriverWait waitUnitView = new WebDriverWait(driver, 20);
			waitUnitView.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
			System.out.println("Select Unit View.. !");
			
			// ***************************************************************
			// Sort columns , Sort by Name ascending 
			WebDriverWait waitNameCol = new WebDriverWait(driver, 20);
			waitNameCol.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout")));
			MobileElement coloumbynameasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout"));
			coloumbynameasc.click();
			System.out.println("Sort Name column ascending .. !");
			// Sort by Name descending
			coloumbynameasc.click();
			System.out.println("Sort Name column desending .. !");
					
			
			// Sort by Type ascending 
			WebDriverWait waitTypeCol = new WebDriverWait(driver, 20);
			waitTypeCol.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout")));
			MobileElement coloumbytypeasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout"));
			coloumbytypeasc.click();
			System.out.println("Sort Type column ascending .. !");
			// Sort by Type descending 
			coloumbytypeasc.click();
			System.out.println("Sort Type column desending .. !");
			
			
			// Access lower menu.
			// Select User from lower menu 
			WebDriverWait waitUsers = new WebDriverWait(driver, 20);
			waitUsers.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView")).click();
			System.out.println("Display Users associated with this account .. !");


			// Access lower menu.
			// Select and display lower Activity
			WebDriverWait waitActivity = new WebDriverWait(driver, 20);
			waitActivity.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")));
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")).click();
			System.out.println("Display Activity associated with this account .. !");
			

			// Access Activity  menu.
			// Select and display All Activity
			WebDriverWait waitAllActivity = new WebDriverWait(driver, 20);
			waitAllActivity.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"All Activity\"]/android.widget.TextView")));
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"All Activity\"]/android.widget.TextView")).click();
			System.out.println("Display All Activity .. !");

			
			// Access Activity  menu.
			// Select and display Unit Activity
			WebDriverWait waitUnitActivity = new WebDriverWait(driver, 20);
			waitUnitActivity.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView")));
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView")).click();
			System.out.println("Display Unit Activity .. !");
			
			
			// Access Activity  menu.
			// Select and display Share Activity
			WebDriverWait waitShareActivity = new WebDriverWait(driver, 20);
			waitShareActivity.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")));
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")).click();
			System.out.println("Display Share Activity .. !");
			
		
			// Exit to home page
			System.out.println("Ending :" + curmod + " test.");
			System.out.println("***************************************");
			sleep(1999);
			//homepagetoexit();
				
		}

		
	}


