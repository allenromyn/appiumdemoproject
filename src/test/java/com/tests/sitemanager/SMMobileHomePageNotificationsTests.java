package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileHomePageNotificationsTests extends SMSamsungGalaxyG9BaseNewLoginClass {
// public class SMMobileHomePageNotificationsTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileHomePageNotificationsTests";
	
	@Test
	public void sMMobileHomePageNotificationsTests() {
		

		System.out.println("Starting :" + curmod + " test.");
		System.out.println("******************************");




		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();

	
	
		// Select Home page notifications.
		sleep(999);
		WebDriverWait waitSelectHomePageNotification = new WebDriverWait(driver, 20);
		waitSelectHomePageNotification.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Select Home Page Notifications.. !");
		sleep(999);
		WebDriverWait waitExitNotification = new WebDriverWait(driver, 20);
		waitExitNotification.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Exit Notifications page.. !");
		

		
		
		
		// Mobile app logout from lower menu.
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("****************************");
		// Exit to home page
		sleep(999);
		//homepagetoexit();
			
	}
	
}
