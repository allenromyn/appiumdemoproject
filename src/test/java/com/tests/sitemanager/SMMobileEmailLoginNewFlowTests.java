package com.tests.sitemanager;



import org.testng.annotations.Test;



public class SMMobileEmailLoginNewFlowTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileEmailLoginTests extends SMSamsungGalaxyG9BaseClass {

	static String curmod = "SMMobileEmailLoginTests";
	
	@Test
	public void sMMobileEmailLoginTests() {
	
		System.out.println("Starting :" + curmod + " test.");
		
		// new login flow. Located in Base class for new login flow and update.
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();

		


		
		
		
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
			
	}
	
	
}


 
 
