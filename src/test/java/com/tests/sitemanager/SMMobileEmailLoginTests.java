package com.tests.sitemanager;


import org.testng.annotations.Test;



public class SMMobileEmailLoginTests extends SMSamsungGalaxyG9BaseNewLoginClass {


	static String curmod = "SMMobileEmailLoginTests";
	@Test
	public void sMMobileEmailLoginTests() {
	
		System.out.println("Starting :" + curmod + " test.");
		System.out.println("***************************************");
		// Login and setup from extended class.
		//**************************************************

		// login bar, login page - from baseclass.
		
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();
		// biometricsApproval << located in base class , this is an if statement.
		
		
		
	
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
		
			
	}
	
}


 
 
