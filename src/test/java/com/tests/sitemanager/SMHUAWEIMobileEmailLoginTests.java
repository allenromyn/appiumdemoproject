package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

public class SMHUAWEIMobileEmailLoginTests extends SMHuaweiBaseClass {
// public class SMMobileEmailLoginTests extends SMHuaweiBaseClass {
	
	
	@Test
	public void sMHUAWEIMobileEmailLoginTests() {
		
		logotap() ;
		System.out.println("Selecting Logo to change to testing dev environment.");
		System.out.println("Starting mobileEmailLoginTests.");
		
		

		// Select and change to dev environment popup
		sleep(1800);
		MobileElement widgetclick = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();

		// Click dev change to dev environment
		MobileElement changetodev = driver.findElement(By.id("android:id/button1"));
		changetodev.click();

		// select login element
		sleep(1800);
		MobileElement mailicon = driver.findElement(By.id("com.noke.nokeaccess:id/email_btn"));
		mailicon.click();

		// enter email
		sleep(1800);
		MobileElement emailenter = driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text"));
		emailenter.sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1800);
		MobileElement password = driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text"));
		password.sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		MobileElement loginarrow = driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle"));
		loginarrow.click();
		System.out.println("login..!");

		// Skip tutorial
		sleep(5000);
		MobileElement skiptutorial = driver.findElement(By.id("com.noke.nokeaccess:id/skip_text"));
		skiptutorial.click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(5000);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(5000);
		MobileElement allowaccesslocation = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");

	
		
		
		
		

	}

	
	}


