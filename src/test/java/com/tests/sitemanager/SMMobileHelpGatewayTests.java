package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SMMobileHelpGatewayTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileFeedbackTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileHelpGatewayTests";
		
	@Test
	public void sMMobileHelpGatewayTests() {

		System.out.println("Starting :" + curmod + " test.");


		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();


		// Access exit page menu
		sleep(999);
		WebDriverWait waitAccessLowerMenu = new WebDriverWait(driver, 20);
		waitAccessLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")).click();
		System.out.println("Select bottom Menu To Exit page .. !");

		// Access exit page Select Gateways 
		sleep(999); //
		WebDriverWait waitSelectGateways = new WebDriverWait(driver, 20);
		waitSelectGateways.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[7]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Gateways, view info.. !"); 
	    sleep(2499);
		driver.navigate().back();


		
		
		
		
		// Exit to home page
		homepagetoexit();
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("******************************************");
			
	}
	
}

