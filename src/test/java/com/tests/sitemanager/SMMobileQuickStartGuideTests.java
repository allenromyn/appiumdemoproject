package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class SMMobileQuickStartGuideTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileQuickStartGuideTests";

	
	
	
	
	@Test
	public void sMMobileQuickStartGuideTests() {
	
		System.out.println("Starting :" + curmod + " test.");
		System.out.println("***************************************");
		// Login and setup from extended class.
		//**************************************************

		// login bar, login page - from baseclass.
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();
		// biometricsApproval << located in base class , this is an if statement.
		
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();

		// Select Quick Start Guide 
		System.out.println("Select Quick Start Guide.");
		WebDriverWait waitSelectQuickStartGuide = new WebDriverWait(driver, 20);
		waitSelectQuickStartGuide.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[12]/android.widget.ImageView\r\n")).click();
		sleep(2499);

	
		// Select NOKE ONE
		System.out.println("Select NOKE ONE.");
		WebDriverWait waitSelectNokeOne = new WebDriverWait(driver, 20);
		waitSelectNokeOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
		// Swipe up to locate logout link.
		sleep(2499);
		driver.navigate().back();
		
		
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");


	
				
	}
	
}


 
 
