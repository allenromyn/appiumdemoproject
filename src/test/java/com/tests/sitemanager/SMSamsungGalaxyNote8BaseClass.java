package com.tests.sitemanager;

import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
/*import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;

public class SMSamsungGalaxyNote8BaseClass {

	static int tc = 13;
	// Login variables 
	static String em = "qa@noke.com";
	static String pw = "password";
	static String si = "Noke Office Test";
	static String ph = "80188814999";
	static String pin = "4545";
//	static String ph = "771793336";
	static String ccode = "+46";
	// Special parms
	static String cmdswipe = "shell input swipe 300 1000 300 300";
	static String cmdwake = "shell input keyevent 26";
	static String cmdUnlock = "adb shell input keyevent 82";
	static String cmdPin = "adb shell input text 4545";
	static String cmdPinwake = "adb shell input keyevent 66";
	static String cmdsleep = "shell input keyevent 26";
	static String testloc = "Noke Office Test";
	static String cmdsvr = "C:\\appium -p 4766";
	
	
	
//	// Samsung Galaxy S9
	static String ip = "10.2.10.184:5555"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "SMG965U"; // device name
	static String uid = "3431313949413098"; // UDID device id
	static String pn = "ANDROID";
	static String prt = "4766";

	
	// Samsung Galaxy Note8
	//static String ip = "10.2.10.184:5555"; // IP address
//	static String pv = "9.0.0"; // platform version SAMSUNG -- ANDROID
//	static String dn = "SM-N950U1"; // device name
//	static String uid = "ce091719442a3e19057e"; // UDID device id
//	static String pn = "ANDROID";
//	static String prt = "4766";

	static String bclass = "SMSamsungGalaxyNote8BaseClass";
	static String extmeth = "SMSamsungGalaxyNote8BaseClass";
	


	static AppiumDriver<MobileElement> driver;

	// Setup starts server , setup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
		System.out.println("Refreshing Appium Server..: " );
		Runtime.getRuntime().exec("taskkill /F /IM node.exe");
		sleep(11999);
		
		
		
		
//		System.out.println("Waking device up..: " + "Name: "+ dn + ", " + "UID: " + uid );
//    	Runtime.getRuntime().exec("adb -s " + uid +  "shell input keyevent 26" );
//		sleep(2199);
//		System.out.println("Swiping up..: " + "Name: "+ dn + ", " + "UID: " + uid );
//		Runtime.getRuntime().exec("adb -s " + uid + "shell input swipe 300 1000 300 300" );
//		sleep(2199);
//		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 82");
//		System.out.println("Unlocking device..: " );
//		sleep(2199);
//		Runtime.getRuntime().exec("adb -s " + uid + " shell input text 4545");
//		System.out.println("Entering PIN..: " + pin);
//		sleep(2199);
//		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 66");
//		System.out.println("Unlocking device with pin..: " );
//		sleep(2199);

		
		
		{		
			
			System.out.println("Waking device up..: " + "Name: "+ dn + ", " + "UID: " + uid );
			Runtime.getRuntime().exec("adb -s " + uid + "shell input swipe 300 1000 300 300" );
			sleep(2199);

		 	System.out.println("Starting Appium server..!"); 
		 	sleep(2199);
			 	
		 		CommandLine cmd = new CommandLine("C:\\Program Files\\nodejs\\node.exe");
		 		System.out.println("Command line entry: C:\\Program Files\\nodejs\\node.exe");
			    sleep(1999);
			    cmd.addArgument("C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    System.out.println("Command line entry: C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    sleep(1999);
			    cmd.addArgument("--address");
			    System.out.println("Command line entry: --address ");
			    sleep(1999);
			    cmd.addArgument("127.0.0.1");
			    System.out.println("Command line entry: 127.0.0.1");
			    sleep(1999);
			    cmd.addArgument("--port");
			    System.out.println("Command line entry: --port ");
			    sleep(1999);
			    cmd.addArgument(prt);
			    System.out.println("Command line entry: " + prt );
			    sleep(1999);
			    cmd.addArgument("--log-level" ,false);
			    cmd.addArgument("error");
			    System.out.println("Loading appium server commands.");
			    sleep(1999);
			    
	
			    
			    
			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(9999);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			    
		 
		 
		sleep(3499);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			sleep(2000);
			
			
			caps.setCapability("appPackage", "com.noke.nokeaccess");
			caps.setCapability("appActivity","com.noke.storagesmartentry.ui.login.LoginActivity");

			URL url = new URL("http://127.0.0.1:4766/wd/hub");
			driver = new AppiumDriver<MobileElement>(url, caps);
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
	
		}
		
		//tap on logo to change testing environment.
		logotap();
		sleep(3000);
		
		
		
		// Basic email login
		emailLoginBasic();

		
		// Biometrics approval no/yes
		sleep(2999);
		biometricsApproval();
		
	
		// Change test location
		//		changeTestLocation();

		
	
		}
	}









	

	
	// logout , stop servers.
	@AfterTest
	public void teardown() throws IOException {
			
		  // Mobile app logout from lower  menu. // 
		  sleep(3999); 
		  System.out.println("Logout .. and reset application..!");
		  System.out.println("Reset App .. !"); //
		  driver.resetApp(); 
		  sleep(3999); 
		  driver.closeApp(); // 
		  sleep(3999); //
		  driver.runAppInBackground(Duration.ofSeconds(0)); // 
		  sleep(3999); //
		  driver.quit();
		  
		
		  Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		  sleep(3999);
		  System.out.println("Android Asleep .. !"); 
		  sleep(3999);
		{
					}
		{
			Runtime runtime = Runtime.getRuntime();
			try {
				
				
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("=======================================================");
				sleep(5999);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	// Change to Noke Office Teat
	// public void changenokeofficetest() {
	
		
		
		
	
	
	
	public void logotap() {
		sleep(2000);
		for (int i = 0; i < tc; i++) {
			// click the button
			driver.findElement(By.id("com.noke.nokeaccess:id/main_logo")).click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
	}
	
	
	public void homepagetoexit() {
		// Go to home page to exit 
		MobileElement selecthome = driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n"));
		selecthome.click();
		System.out.println("Exit page.. !");
		sleep(4500);
	}


	
	public void skipTutorial() {
		// Skip tutorial
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Skip tutorial.");
	}

	
	
	
	public void changetoDevEnv() {
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		// Click dev change to dev environment 
		driver.findElement(By.id("android:id/button1")).click();
		
	}		
	
	
//  workking biometric by itself w no if statement 	
//  do not save biometrics - just verifies message.	Note: Enable acts as if the oppisite.
	public void biometricsApproval() {
	//	do not save biometrics - just verifies message.	
	sleep(4999);
	if(!driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).isEnabled()) {
		System.out.println("No  biometrics Settings available..!");
		sleep(4999);		
	} else
	  {
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
		sleep(4999);
		System.out.println("Do not save biometrics settings..!");
	  }
	}
	

	
		
//	public void biometricsApproval() {
//	//	do not save biometrics - just verifies message.	
//	sleep(3000);
//	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
//	System.out.println("Do not save biometrics -login.. !");
//	sleep(14999);
//	}
//

		
		
	
	
	public void emailLoginBasic() {
		// Select and change to dev environment popup
		sleep(1900);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

		// Click dev change to dev environment
		driver.findElement(By.id("android:id/button1")).click();

		// select login element
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();

		// enter email
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		System.out.println("login..!");
	}

	
	public void changeTestLocation() {
		// Change test location to Noke Office test
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Select dropdown to change test location.");
		// Select spyglass 
		sleep(4999);
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
		System.out.println("");
		// Enter location 
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(testloc);
		sleep(999);
		System.out.println("Enter test location: " + testloc + " ..!" );
		// Select test location
		sleep(9999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select  test location.");
	}

	
	public void changeTestSite() {
		// Select change test site - Major test site change .
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Select dropdown to change test site.. !");
		// Select change test site spyglass
		sleep(4999);
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
		System.out.println("Select search-spyglass to change test site.. !");
		// Enter test site name
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(si);
		System.out.println("Select search-spyglass to change test site.. !");
		// Select search value
		sleep(6999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
		System.out.println("Select test site.. !");
	}
	
	public void clearFilter() {
		// Clear filter method
		sleep(4444);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Clear filter.. !");
		
	}

	
	
	
	public void locationAccessDevice() {
		// Select and set location popup
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button\r\n")).click();
		System.out.println("Select Ok accept location popup.");
		// Select Allow access
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")).click();
		System.out.println("Allow access to application.. !");
	}

	
	
	public void selectUnitsFromMenu() {
		// Select Units page
		sleep(5999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Unit page.. !");
	}

	
	
	
	
	
	
	
	public static void exitFilterPage() {
		MobileElement selectfiltersxout = driver.findElement(By.id("com.noke.nokeaccess:id/close"));
		selectfiltersxout.click();
		System.out.println("Exit Filter page.. !");
		sleep(4000);
	}
	
	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
