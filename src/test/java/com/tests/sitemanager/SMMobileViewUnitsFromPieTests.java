package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


//public class SMMobileViewUnitsFromPieTests extends SMSamsungGalaxyG9BaseClass {
public class SMMobileViewUnitsFromPieTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	String curmod = "SMMobileViewUnitsFromPieTests";
	
	
	
	@Test
	public void sMMobileViewUnitsFromPieTests() {
		
		System.out.println("Starting :" + curmod + " test.");
		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location
		changeTestLocation();

	
		// Enter select pie chart
		WebDriverWait waitSelectPieChart = new WebDriverWait(driver, 20);
		waitSelectPieChart.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView")).click();
		System.out.println("Select Pie chart.. !");
		sleep(2999);
		
		
		// Enter select units
		WebDriverWait waitDisplayUnit = new WebDriverWait(driver, 20);
		waitDisplayUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button")).click();
		sleep(2999);
		System.out.println("Select Units to display.. !");
			
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
			
	}

	
}




	



