package com.tests.sitemanager;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/*import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
public class SMMobileSettingsHelpSupportLinkTests extends SMSamsungGalaxyG9BaseNewLoginClass {

	static String curmod = "SMMobileSettingsHelpSupportLinkTests";

	@Test
	public void sMMobileSettingsHelpSupportLinkTests() {
	
		
		System.out.println("Starting :" + curmod + " test.");
		
	   	// Skip tutorial 
		skipTutorial();
    	//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();

		// select lover menu.
		WebDriverWait waitLowerMenu = new WebDriverWait(driver, 20);
		waitLowerMenu.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Help & Support  .. !");
		sleep(699);
		// Access Help and Support
		helpAndSupportLink();

		// Access Help and Support FAQ
		WebDriverWait waitHelpAndSupportFaq = new WebDriverWait(driver, 20);
		waitHelpAndSupportFaq.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
		System.out.println("Select FAQ Link  .. !");
		sleep(1999);
		driver.navigate().back();
		// Access Help and Support
		helpAndSupportLink();
		
		
		// Access Help and Support View Tenant Support
		WebDriverWait waitTenantSupport = new WebDriverWait(driver, 20);
		waitTenantSupport.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")).click();
		System.out.println("Select View Tenant Support Link  .. !");


			// Access Help and Support View Tenant Support start here.
			WebDriverWait waitTenantSupporStartHere = new WebDriverWait(driver, 20);
			waitTenantSupporStartHere.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]\r\n")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]\r\n")).click();
			System.out.println("Select View Tenant Support-Start Here Link  .. !");
			sleep(1999);
			driver.navigate().back();
		

			// Access Help and Support View Tenant Support Open Gate Door.
			WebDriverWait waitTenantSupportOpenGate = new WebDriverWait(driver, 20);
			waitTenantSupportOpenGate.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]")).click();
			System.out.println("Select View Tenant Support-Open Gate Door Link  .. !");
			sleep(1999);
			driver.navigate().back();
	
		
			// Access Help and Support View Tenant Support Unlocking your unit
			WebDriverWait waitSupportUnlockUnit = new WebDriverWait(driver, 20);
			waitSupportUnlockUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]")).click();
			System.out.println("Select View Tenant Support-Unlocking Your Unit  .. !");
			sleep(699);
			driver.navigate().back();
			sleep(1999);
		
		
			// Access Help and Support View Tenant Support locking your unit
			WebDriverWait waitSupporLockingUnit = new WebDriverWait(driver, 20);
			waitSupporLockingUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]")).click();
			System.out.println("Select View Tenant Support-Locking Your Unit  .. !");
			sleep(699);
			driver.navigate().back();
			sleep(1999);
		
		
			// Access Help and Support View Tenant Support Sharing a Didgital Key
			WebDriverWait waitSupporSharingDigital = new WebDriverWait(driver, 20);
			waitSupporSharingDigital.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]")).click();
			System.out.println("Select View Tenant Support-Sharing a Digital Key  .. !");
			sleep(699);
			driver.navigate().back();
			sleep(1999);
		
			// Access Help and Support View Tenant Support Sharing a Make a Payment
			WebDriverWait waitMakingPayment = new WebDriverWait(driver, 20);
			waitMakingPayment.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]")).click();
			System.out.println("Select View Tenant Support-Making A Payment Not funtional for now  .. !");
			sleep(1999);
			driver.navigate().back();

			// Access Help and Support View Tenant Support Contact Us
			// Generic contact us link. located in base class.	
			contactUs();
			//**********
				// Call Us
				WebDriverWait waitHelpSupportCallUs2 = new WebDriverWait(driver, 20);
				waitHelpSupportCallUs2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")));
				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]\r\n")).click();
				System.out.println("Select View Tenant Support- Contact Us - Call Us.. !");
				sleep(1999);
					// Select Deny Call Access
					WebDriverWait waitHelpSupportCallUsDeny = new WebDriverWait(driver, 20);
					waitHelpSupportCallUsDeny.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[2]\r\n")));
					driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[2]\r\n")).click();
					System.out.println("Select View Tenant Support- Contact Us - Call Us - Deny permission.. !");
				sleep(1999);
				driver.navigate().back();

//				// Submit Support Ticket
//				WebDriverWait waitHelpSupportSubmitTicket = new WebDriverWait(driver, 20);
//				waitHelpSupportSubmitTicket.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")));
//				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")).click();
//				System.out.println("Select View Tenant Support- Contact Us - Submit Support Ticket.. !");
//				sleep(1999);
//				driver.navigate().back();
					
				// Access Help and Support View Tenant Support Contact Us
				// Generic contact us link. located in base class.	
				// Cancel
//				WebDriverWait waitHelpSupportCancel = new WebDriverWait(driver, 20);
//				waitHelpSupportCancel.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[4]\r\n")));
//				driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[4]\r\n")).click();
//				System.out.println("Select View Tenant Support- Contact Us - Cancel.. !");
//				sleep(1999);
//				driver.navigate().back();
	
		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");
	
	
	
		}


}
		



	



