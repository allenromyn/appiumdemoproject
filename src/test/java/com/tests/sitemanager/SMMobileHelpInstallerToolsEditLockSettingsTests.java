package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileHelpInstallerToolsEditLockSettingsTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileInstallerToolsEditLockSettingsTests";
	static String sunit = "1005";
	static String ftime = "3";
	
	@Test
	public void sMMobileInstallerToolsEditLockSettingsTests() {
	
		

		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	

		// select lover menu. Units
		WebDriverWait waitSelectUnits = new WebDriverWait(driver, 20);
		waitSelectUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select from bottom Menu Units..!");
		sleep(1299);

		// Select search spy glass
		WebDriverWait waitSelectGlassSearch = new WebDriverWait(driver, 20);
		waitSelectGlassSearch.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")).click();
		System.out.println("Select spy glass search.. !");
		sleep(1299);

		// Select Enter search criteria --  sunit
		WebDriverWait waitSearchUnit = new WebDriverWait(driver, 20);
		waitSearchUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. !");
		sleep(1299);
			
		// Select Enter search criteria - returned results
		WebDriverWait waitUnitInfo = new WebDriverWait(driver, 20);
		waitUnitInfo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
		System.out.println("View unit info.. !");
		sleep(1299);

		
		// Access the Locks from unit detail
		WebDriverWait waitAccessLocks = new WebDriverWait(driver, 20);
		waitAccessLocks.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")).click();
		System.out.println("Access Locks .. !");
		sleep(1299);
		
		// Access the unit detail to edit / unit name
		WebDriverWait waitUnitDetail = new WebDriverWait(driver, 20);
		waitUnitDetail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Access Unit detail to edit..!");
		sleep(1299);
		
		
		// swipe from bottom to top - 
		// bring advance settings into view 
		swipeBottomToTop();
		
		
		
		// Select Advance Settings
		WebDriverWait waitAdvancedSettings = new WebDriverWait(driver, 20);
		waitAdvancedSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[8]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Advanced Settings..!");
		sleep(1299);
		
		// Select Fire Time seconds
		WebDriverWait waitSelectEdit = new WebDriverWait(driver, 20);
		waitSelectEdit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.Button\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.Button\r\n")).click();
		System.out.println("Select Edit..!");
		sleep(1299);
		
		// Edit Fire Time seconds
		WebDriverWait waitEnterNewValue = new WebDriverWait(driver, 20);
		waitEnterNewValue.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.EditText\r\n")).clear();
		sleep(1299);
		WebDriverWait waitEnterNewValueMore = new WebDriverWait(driver, 20);
		waitEnterNewValueMore.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.EditText")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.EditText")).sendKeys(ftime);
		System.out.println("Enter new value ..!");
		
		
		// Save edit
		sleep(1299);
		WebDriverWait waitSelectSave = new WebDriverWait(driver, 20);
		waitSelectSave.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.Button")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.Button")).click();
		System.out.println("Select Save..!");
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		
		

		//*****************************************************
		// Select search spy glass
		WebDriverWait waitSelectSpyGlass = new WebDriverWait(driver, 20);
		waitSelectSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select spy glass search.. !");
		sleep(1299);
		// Select Enter search criteria
		WebDriverWait waitSearchFor = new WebDriverWait(driver, 20);
		waitSearchFor.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. !");
		sleep(1299);
			
		// Select Enter search criteria
		WebDriverWait waitViewUnitInfo = new WebDriverWait(driver, 20);
		waitViewUnitInfo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
		System.out.println("View unit info.. !");
		sleep(1299);
		
		// swipe from bottom to top - 
		// bring advance settings into view 
		swipeBottomToTop();
		


		// Access the Locks from unit detail
		WebDriverWait waitAccessLocksLast = new WebDriverWait(driver, 20);
		waitAccessLocksLast.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[11]/android.widget.ImageView\r\n")).click();
	    System.out.println("Access Locks - info.. !");
		sleep(1299);
		

		
		// Access the unit detail to edit
		WebDriverWait waitAccessUnitDetail = new WebDriverWait(driver, 20);
		waitAccessUnitDetail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
		System.out.println("Access Unit detail to edit..!");
		sleep(1299);
		
		


		// Select Advance Settings
		WebDriverWait waitAdvancedSettingsLast = new WebDriverWait(driver, 20);
		waitAdvancedSettingsLast.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Advanced Settings..!");
		sleep(1299);
		System.out.println("Select Advanced Settings..!");
		sleep(1299);
		System.out.println("Verify change was saved..!");
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();
		sleep(1299);
		driver.navigate().back();

		
		
		
		
		
			
		
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");
		

	
	
		}

	
	}
		



	



