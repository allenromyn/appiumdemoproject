package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobileViewLocateUnitFromUnitDetailTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String sunit = "1007";
	static String curmod = "SMMobileViewLocateUnitFromUnitDetailTests";

	
	
	
	@Test
	public void sMMobileViewLocateUnitFromUnitDetailTests() {

		sleep(3999);
		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();



		
	
		// Select Units page from lower menu.
		sleep(3999);
		WebDriverWait waitSelectUnit = new WebDriverWait(driver, 20);
		waitSelectUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Unit page.. !");

		// Select search spy glass
		sleep(3999);
		WebDriverWait waitSelectSearchSpyGlass = new WebDriverWait(driver, 20);
		waitSelectSearchSpyGlass.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")));
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter search criteria
		sleep(3999);
		WebDriverWait waitEnterUnitSearch = new WebDriverWait(driver, 20);
		waitEnterUnitSearch.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. ! "+sunit );

		// Select view unit info
		WebDriverWait waitViewUnitInfo = new WebDriverWait(driver, 20);
		waitViewUnitInfo.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("View unit info.. !");
		sleep(3999);
	
		// Unlock lock from unit detail
		sleep(3999);
		WebDriverWait waitViewUnitDetail = new WebDriverWait(driver, 20);
		waitViewUnitDetail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.Button\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.Button\r\n")).click();
		System.out.println("Select Locate unit from unit info.. !");
		// Select popup Remote Unlock message 
		sleep(5999);
//		WebDriverWait waitYestoUnlock = new WebDriverWait(driver, 20);
//		waitYestoUnlock.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")));
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]\r\n")).click();
//		System.out.println("Select Yes to unlock.. !");
// 		sleep(1999);
// 		driver.navigate().back();
// 		sleep(1999);
 		driver.navigate().back();
 		driver.navigate().back();
 		driver.navigate().back();

		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
		System.out.println("****************************");
		
			
	}

	
}

