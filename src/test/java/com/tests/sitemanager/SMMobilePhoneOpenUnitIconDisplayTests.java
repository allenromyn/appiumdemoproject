package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SMMobilePhoneOpenUnitIconDisplayTests extends SMSamsungGalaxyG9BaseNewLoginClass {
// public class SMMobileEmailLoginTests extends SMHuaweiBaseClass {
	static String curmod = "SMMobilePhoneOpenUnitIconDisplayTests";
	
	@Test
	public void sMMobilePhoneOpenUnitIconDisplayTests() {
		
		System.out.println("Starting :" + curmod + " test.");
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
	    // Changes test site from home page
		// Modual located in BaseClass -- generic test site change.
		System.out.println("Site Manager Home Page..!");
		// Change test location
		sleep(999);
		changeTestLocation();

		
			
		// Select Units from lower menu
		sleep(999);
		WebDriverWait waitUnits = new WebDriverWait(driver, 20);
		waitUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Units from menu.. !");

		
	
		
		
		// Exit to home page
		// homepagetoexit();
		
		sleep(999);
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("*****************************");

		}
	
	}


