package com.tests.sitemanager;



import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class SMMobileShowFirmwareVersionTests extends SMSamsungGalaxyG9BaseNewLoginClass {
// public class SMMobileShowFirmwareVersionTests extends SMSamsungGalaxyG9BaseClass {
	String sunit = "1004";
	static String curmod  = "SMMobileShowFirmwareVersionTests";
	
	
	@Test
	public void sMMobileShowFirmwareVersionTests() {
		System.out.println("Starting :" + curmod + " test.");
	
		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	
	

		// Select Units page via 
		sleep(3999);
		WebDriverWait waitSelectUnits = new WebDriverWait(driver, 20);
		waitSelectUnits.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Unit page from lower menu.. !");


		// Select Enter search for available unit
		sleep(3999);
		System.out.println("Select spy glass.. !");
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")).click();
		WebDriverWait waitSearchUnit = new WebDriverWait(driver, 20);
		waitSearchUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(sunit);
		System.out.println("Enter Unit to search.. !");
	
		
			// Select searched  available unit 
			sleep(3999);
			WebDriverWait waitSearchedReturnUnit = new WebDriverWait(driver, 20);
			waitSearchedReturnUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")));
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
			System.out.println("Select unit - returned search.. !");
			sleep(3999);
			driver.navigate().back();
			sleep(3999);
			driver.navigate().back();
	

			

			
			// Exit to home page
			// Select home from lower menu 
			System.out.println("Select Home icon  - viw lower menu.. !");
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
			sleep(3999);	

			System.out.println("Ending :" + curmod + " test.");
			System.out.println("***************************************");
			sleep(1999);
			// Select More - lower right corner - Exit swipe up - slide 
			selectMoreLowerRight();
			// Swipe up to locate logout link.
			swipeBottomToTop();
			// Select logout
			logoutWithPopup();
				
		}

	
	}


