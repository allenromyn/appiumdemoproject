package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SMMobileSettingsAccountTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsAccountTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod = "SMMobileSettingsAccountTests";
	
	
	@Test
	public void sMMobileSettingsAccountTests() {
		

		System.out.println("Starting :" + curmod + " test.");
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	

		
		
		
		// Go to exit page to logout / Bottom menu
		WebDriverWait waitHomeExit = new WebDriverWait(driver, 20);
		waitHomeExit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Oprn Exit page / Lower Menu .. !");
		sleep(999);
		
		
		// Select Account
		WebDriverWait waitAccountDetail = new WebDriverWait(driver, 20);
		waitAccountDetail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Open  Account menu..!");
		sleep(999);
		driver.navigate().back();
		
		
		
		
		
		
		// Exit to home page
		//homepagetoexit();
		// Mobile app logout from lower menu.
		sleep(999);
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("****************************");
			
	}
	
}

	

