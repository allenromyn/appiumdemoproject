package com.tests.sitemanager.note5;


/*import java.time.Duration;*/

import org.openqa.selenium.By;
/*import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

public class SMMobileNote5SettingsHelpSupportLinkTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String curmod = "SMMobileNote5SettingsHelpSupportLinkTests";

	
		
		@Test
		public void sMMobileNote5SettingsHelpSupportLinkTests() {
			
			System.out.println("Starting : " + curmod);
			sleep(1999);


			
			
			
			
			
			// skit tutorial
			skipTutorial();
			sleep(999);
			
			// Select popup and Allow Access
			selectLocationAllowAccess();
			sleep(999);
		
			// Change to Noke Office Test
			ChangeToNokeOfficeTest();
			sleep(999);

		// select lover menu.
		sleep(4999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom Menu to access Help & Support  .. !");

		// Access Help and Support
		sleep(4999);
		MobileElement selecthelpandsupport = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.ImageView\r\n"));
		selecthelpandsupport.click();
		System.out.println("Select Help & Support Link  .. !");

		// Access Help and Support FAQ
		sleep(4999);
		MobileElement selecthelpandsupportfaq = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
		selecthelpandsupportfaq.click();
		System.out.println("Select FAQ Link  .. !");
		sleep(6000);
		driver.navigate().back();
		sleep(2000);
		selecthelpandsupport.click();

		// Access Help and Support View Tenant Support
		sleep(4999);
		driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]")).click();
		System.out.println("Select View Tenant Support Link  .. !");

		// Access Help and Support View Tenant Support start here.
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]")).click();
		System.out.println("Select View Tenant Support-Start Here Link  .. !");
		sleep(4999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Open Gate Door.
		MobileElement supportviewtenantgatedoor = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]"));
		supportviewtenantgatedoor.click();
		System.out.println("Select View Tenant Support-Open Gate Door Link  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Unlocking your unit
		sleep(4999);
		MobileElement supportunlockingunit = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]"));
		supportunlockingunit.click();
		System.out.println("Select View Tenant Support-Unlocking Your Unit  .. !");
		sleep(4999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support locking your unit
		sleep(4999);
		MobileElement supportlockingunit = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]"));
		supportlockingunit.click();
		System.out.println("Select View Tenant Support-Locking Your Unit  .. !");
		sleep(2000);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Didgital Key
		sleep(4999);
		MobileElement supportsharingdkey = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]"));
		supportsharingdkey.click();
		System.out.println("Select View Tenant Support-Sharing a Digital Key  .. !");
		sleep(2000);
		driver.navigate().back();

	  
		// Exit to home page
		sleep(4999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView\r\n")).click();

//		System.out.println("Exit to home page..!");
//		homepagetoexit();

			
		sleep(2000);
		System.out.println("Ending :" + curmod);
	
	
		}

	
	}
		



	



