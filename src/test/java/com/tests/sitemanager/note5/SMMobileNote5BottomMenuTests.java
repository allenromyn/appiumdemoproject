package com.tests.sitemanager.note5;


import org.openqa.selenium.By;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;


public class SMMobileNote5BottomMenuTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String curmod = "SMMobileNote5BottomMenuTests";
	
	
	
	
	@Test
	public void sMMobileBottomMenuTests() {

		
		
		sleep(2900);
		System.out.println("Starting : " + curmod);

			
			// Skip tutorial 
			sleep(4900);
			driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
			System.out.println("Skip tutorial.");
			

			// Select popup and Allow Access
			selectLocationAllowAccess();
			

			// Select View Units from menu
			sleep(4900);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
			System.out.println("Select Unit View.. !");
			
			// ***************************************************************
			// Sort columns , Sort by Name ascending 
			sleep(4900);
			MobileElement coloumbynameasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout"));
			coloumbynameasc.click();
			System.out.println("Sort Name column ascending .. !");
			// Sort by Name descending
			sleep(3900);
			coloumbynameasc.click();
			System.out.println("Sort Name column desending .. !");
					
			
			// Sort by Type ascending 
			sleep(4900);
			MobileElement coloumbytypeasc = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout"));
			coloumbytypeasc.click();
			System.out.println("Sort Type column ascending .. !");
			// Sort by Type descending 
			sleep(3900);
			coloumbytypeasc.click();
			System.out.println("Sort Type column desending .. !");
			
			
			// Access lower menu.
			// Select User from lower menu 
			sleep(4900);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView")).click();
			System.out.println("Display Users associated with this account .. !");


			// Access lower menu.
			// Select and display lower Activity
			sleep(4900);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")).click();
			System.out.println("Display Activity associated with this account .. !");
			

			// Access Activity  menu.
			// Select and display All Activity
			sleep(4900);
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"All Activity\"]/android.widget.TextView")).click();
			System.out.println("Display All Activity .. !");

			
			// Access Activity  menu.
			// Select and display Unit Activity
			sleep(4900);
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView")).click();
			System.out.println("Display Unit Activity .. !");
			
			
			// Access Activity  menu.
			// Select and display Share Activity
			sleep(4900);
			driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")).click();
			System.out.println("Display Share Activity .. !");
			
		
			
			sleep(2900);
			System.out.println("Ending : " + curmod);

			
			

		}


	
	}


