package com.tests.sitemanager.note5;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileNote5SearchforUnitsTests extends SMSamsungGalaxyNote5EmailBaseClass {

	static String sunit = "3013";

	@Test
	public void sMMobileSearchforUnitsTests() {


		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();

		// Select Units page
		sleep(5999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Unit page.. !");

		// Select search spy glass
		sleep(5999);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter searcj criteria
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
		System.out.println("Enter unit to search for.. !");

		// Select Enter searcj criteria
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
		System.out.println("View unit info.. !");
		sleep(5999);
		driver.navigate().back();
		sleep(5999);
		driver.navigate().back();

		sleep(2000);
		System.out.println("End SMMobileSearchforUnitsTests.");

	}

}
