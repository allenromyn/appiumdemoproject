package com.tests.sitemanager.note5;

import org.openqa.selenium.By;

import org.testng.annotations.Test;


public class SMMobileNote5SettingsAdvancedTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String curmod = "SMMobileNote5SettingsAdvancedTests";

	
	@Test
	public void sMMobileNote5SettingsAdvancedTests() {
		
		System.out.println("Starting : " + curmod);
		sleep(1999);


		
		
		
		
		
		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
		sleep(999);


		
		
		// Access exit page menu
		sleep(3999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView")).click();
		System.out.println("Select bottom Menu.. !");

		// Access exit page menu select advanced settings
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Advanced Settings.. !");
		sleep(1500);
		driver.navigate().back();


		// Mobile app logout from lower menu.
		sleep(2000);
		System.out.println("Ending : " + curmod);

	}
}
