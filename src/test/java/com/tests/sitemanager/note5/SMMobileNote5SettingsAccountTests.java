package com.tests.sitemanager.note5;


import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileNote5SettingsAccountTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String curmod = "SMMobileNote5SettingsAccountTests";
	
	
	@Test
	public void sMMobileNote5SettingsAccountTests() {
		
		System.out.println("Starting : " + curmod);
		sleep(1999);


		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
		sleep(999);


			

	
		// Select Bottom menu
		sleep(3999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select bottom menu..!");
		
		
		// Select Account
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView\r\n")).click();
		System.out.println("Open  Account menu..!");
		sleep(3999);
		driver.navigate().back();
		sleep(3999);
		driver.navigate().back();
		
		
		
		
		
		
		sleep(2999);
		System.out.println("Ending : " + curmod);
	
		
			

		}

	
	}


