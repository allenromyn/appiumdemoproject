package com.tests.sitemanager.note5;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileNote5ViewUnitDetailTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String sunit = "1005";
	static String curmod = "SMMobileNote5ViewUnitDetailTests";

	
	
	
	@Test
	public void sMMobileNote5ViewUnitDetailTests() {


		sleep(6500);
		System.out.println("Starting : " + curmod);

		
		
		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
		sleep(999);

		// Select Units page
		sleep(6500);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
		System.out.println("Select Unit page.. !");

		// Select seatch spy glass
		sleep(6500);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
		System.out.println("Select spy glass search.. !");

		// Select Enter search criteria
		sleep(6500);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
		System.out.println("Enter unit to search for: " + sunit );

		// Select Enter searcj criteria
		sleep(6500);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
		System.out.println("View unit info.. !");
		sleep(5000);
		driver.navigate().back();
		sleep(5000);
		driver.navigate().back();
		
		// homepagetoexit();
		
		sleep(6500);
		System.out.println("Ending : " + curmod );

	}

}
