package com.tests.sitemanager.note5;

import java.io.IOException;

import org.openqa.selenium.By;

import org.testng.annotations.Test;


public class SMMobileNote5ActivitySortingTests extends SMSamsungGalaxyNote5EmailBaseClass {

	static String curmod = "SMMobileNote5ActivitySortingTests";
	
	
	
	
	@Test
	public void sMMobileNote5ActivitySortingTests() throws IOException {

		
		System.out.println("Starting : " + curmod);

		// skit tutorial
		skipTutorial();

		// Select popup and Allow Access
		selectLocationAllowAccess();
		
		
		

		// Select Activity
		sleep(3999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Activity\"]/android.widget.ImageView")).click();
		System.out.println("Select Activity column.. !");

		// Select Activity - Units column
		sleep(3999);
		driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Units\"]/android.widget.TextView")).click();
		System.out.println("Select Units column.. !");

		// Select Activity - Share column
		sleep(3999);
		driver.findElement(By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Share\"]/android.widget.TextView")).click();
		System.out.println("Select Share column.. !");

		// Exit to home page
		sleep(3999);
		homepagetoexit();

		// Mobile app logout from lower menu.
		sleep(2999);
		System.out.println("Ending : " + curmod);

	}

}
