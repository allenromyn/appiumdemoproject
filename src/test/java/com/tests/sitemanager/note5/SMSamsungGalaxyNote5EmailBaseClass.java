package com.tests.sitemanager.note5;

import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.By;
/*import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;*/
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.net.URL;

public class SMSamsungGalaxyNote5EmailBaseClass {

	static int tc = 13;
	static String em = "qa@noke.com";
	static String pw = "password";
	static String si = "Noke Office Test";
	static String ph = "8018375908";
//  static String ph = "8018889999";
	static String pin = "4545";
//	static String ph = "771793336";
	static String ccode = "+46";	// Special parms
	static String cmdwake = "adb shell input keyevent 26";
	static String cmdUnlock = "adb shell input keyevent 82";
	static String cmdPin = "adb shell input text 1234";
	static String cmdPinwake = "adb shell input keyevent 66";
	static String cmdsleep = "adb shell input keyevent 26";	
	static String cmdsvr = "C:\\appium -p 4766";
	// Samsung Galaxy Note 5
	static String ip = "10.2.10.163:5555"; // IP address
	static String pv = "8.0.0"; // platform version SAMSUNG -- ANDROID
	static String dn = "SMN920T"; // device name
	static String uid = "0715f7bd0c5d093a"; // UDID device id
	static String pn = "ANDROID";
	static String prt = "4766";
	static String bclass = "SMSamsungGalaxyNote5BaseClass";


	
	


	static AppiumDriver<MobileElement> driver;

	// Setup starts server , setup Desired Capabilities.
	@BeforeTest
	public void setup() throws IOException {
	
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		System.out.println("Waking device up..: " + "Name: "+ dn + ", " + "UID: " + uid );
		sleep(2999);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 82");
		System.out.println("Unlocking device..: " );
		sleep(2999);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input text 4545");
		System.out.println("Entering PIN..: " + pin);
		sleep(2999);
		Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 66");
		System.out.println("Unlocking device with pin..: " );
		sleep(2999);

		
		{		
					  
		 	System.out.println("Starting Appium server..!"); 
		 	sleep(2000);
			 	
		 		CommandLine cmd = new CommandLine("C:\\Program Files\\nodejs\\node.exe");
		 		System.out.println("Command line entry: C:\\Program Files\\nodejs\\node.exe");
			    sleep(1299);
			    cmd.addArgument("C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    System.out.println("Command line entry: C:\\Users\\noke\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
			    sleep(1299);
			    cmd.addArgument("--address");
			    System.out.println("Command line entry: --address ");
			    sleep(1299);
			    cmd.addArgument("127.0.0.1");
			    System.out.println("Command line entry: 127.0.0.1");
			    sleep(1299);
			    cmd.addArgument("--port");
			    System.out.println("Command line entry: --port ");
			    sleep(1299);
			    cmd.addArgument(prt);
			    System.out.println("Command line entry: " + prt );
			    sleep(1299);
			    cmd.addArgument("--log-level" ,false);
			    cmd.addArgument("error");
			    System.out.println("Loading appium server commands.");
			    sleep(1299);
			    
	
			    
			    
			    DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
			    DefaultExecutor executor = new DefaultExecutor();
			    executor.setExitValue(1);
			    try {
			        executor.execute(cmd, handler);
			        Thread.sleep(9999);
			    } catch (IOException | InterruptedException e) {
			        e.printStackTrace();
			    }
			    
		 
		 
		sleep(1999);
		try {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, pn);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, pv);
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, dn);
			caps.setCapability(MobileCapabilityType.UDID, uid);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			sleep(1999);
			caps.setCapability("appPackage","com.noke.nokeaccess");
			caps.setCapability("appActivity","com.noke.storagesmartentry.ui.login.LoginActivity");

			URL url = new URL("http://127.0.0.1:4766/wd/hub");
			driver = new AppiumDriver<MobileElement>(url, caps);
			
		} catch (Exception exp) {
			System.out.println("Cause is : " + exp.getCause());
			System.out.println("Message is : " + exp.getMessage());
			exp.printStackTrace();
	
		}
		logotap();
		sleep(1999);
		
		
		// Basic email login
		emailLoginBasic();


		
		// Biometrics approval no/yes
		// biometricsApproval();
	
		
	
		}
	}



	

	
	// logout , stop servers.
	@AfterTest
	public void teardown() throws IOException {
			
		  // Mobile app logout from lower  menu. // 
		  sleep(2999); 
		  System.out.println("Logout .. and reset application..!");
		  driver.resetApp();
		  System.out.println("Reset App..!"); //
		  sleep(2999); 
		  driver.closeApp(); // 
		  System.out.println("Close App..!"); //
		  sleep(2999); //
		  driver.runAppInBackground(Duration.ofSeconds(0)); // 
		  sleep(2999); //
		  System.out.println("End driver..!"); //
		  driver.quit();
		  
		
		  Runtime.getRuntime().exec("adb -s " + uid + " shell input keyevent 26");
		  sleep(2999);
		  System.out.println("Android Asleep .. !"); 
		  sleep(2999);
		{
					}
		{
			Runtime runtime = Runtime.getRuntime();
			try {
				
				
				runtime.exec("taskkill /F /IM node.exe");
				System.out.println("Closing server..!");
				System.out.println("===============================================");
				sleep(2999);
				
			} catch (IOException e) {
				e.printStackTrace();

			}

		}

	}

	
	
	
	
	
	
	
	
	// Change to Noke Office Teat
		public void changenokeofficetest() {
	
		
		}
		
	
	
	
	public void logotap() {
		sleep(3000);
		for (int i = 0; i < tc; i++) {
			// click the button
			MobileElement logochangetodev = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView\r\n"));
			logochangetodev.click();
			// wait 1/10 seconds

			// check that data is being generated correctly
		} 		System.out.println("Selecting Logo to change to testing dev environment.");
				sleep(1999);
	}
	
	
	public void homepagetoexit() {
		// Go to home page to exit 
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Home page .. !");
		sleep(4500);
	}

	
	public void skipTutorial() {
		// Skip tutorial 
		sleep(4500);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]\r\n")).click();
		System.out.println("Skip tutorial.");
	}	
	
	
	public void changetoDevEnv() {
		MobileElement widgetclick =  driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout"));
		widgetclick.click();
		// Click dev change to dev environment 
		driver.findElement(By.id("android:id/button1")).click();
		
	}		
		
	
	public void biometricsApproval() {
		// do not save biometrics - just verifies message.
		sleep(9999);
		MobileElement biosave = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]\r\n"));
		biosave.click();
		System.out.println("Do not save biometrics -login.. !");
	}
	
	
	public void emailLoginBasic() {
		// Select and change to dev environment popup
		sleep(1900);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();

		// Click dev change to dev environment
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[3]\r\n")).click();

		// select login element
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_btn")).click();

		// enter email
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/email_edit_text")).sendKeys(em);
		System.out.println("Enter email as: " + em);

		// enter password
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");

		// login arrow
		sleep(1900);
		driver.findElement(By.id("com.noke.nokeaccess:id/text_input_password_toggle")).click();
		System.out.println("login..!");
	}

	
	
	public void ChangeToNokeOfficeTest() {
		// Change to Noke Office Test 
		// Change to store Noke Office Test.
		sleep(4499);
		System.out.println("Select Test Location..!");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.ImageView\r\n")).click();
		sleep(4499);
		driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n")).click();
		sleep(4499);
		MobileElement enternokeofficetest = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n"));
		enternokeofficetest.click();
		sleep(4499);
		enternokeofficetest.sendKeys(si);
		System.out.println("Changed to Noke Office Test..!");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.TextView\r\n")).click();
	}

	
	
	
	public void selectLocationAllowAccess() {
		// Select and set location popup
		sleep(4900);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4900);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");
	}
	
	
	
	public static void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
