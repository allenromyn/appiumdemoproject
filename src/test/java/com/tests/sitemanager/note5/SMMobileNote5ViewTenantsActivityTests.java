package com.tests.sitemanager.note5;

import org.openqa.selenium.By;
import org.testng.annotations.Test;






public class SMMobileNote5ViewTenantsActivityTests extends SMSamsungGalaxyNote5EmailBaseClass {	
	static String curmod = "SMMobileNote5ViewTenantsActivityTests";
	
	
	
	@Test
	public void sMMobileViewTenantsActivityNote5Tests() {
		
		System.out.println("Stating : " + curmod);
		
		
		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
		sleep(999);
		
		
		
		// Select View Users - Tenants
		sleep(5999);
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Users\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Select User icon from lower menu.. !");

		// Select User from list-
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Select User from list.. !");

		// Select Units
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView\r\n")).click();
		System.out.println("Select User - Display page.. !");
		sleep(5999);
		driver.navigate().back();
		sleep(5999);
		driver.navigate().back();


		sleep(5999);
		System.out.println("Ending : " + curmod);

	}

	

}
