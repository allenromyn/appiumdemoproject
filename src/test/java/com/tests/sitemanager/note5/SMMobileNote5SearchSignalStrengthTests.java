package com.tests.sitemanager.note5;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


// public class SMMobileNote5SearchSignalStrengthTests extends SMSamsungGalaxyNote5EmailBaseClass {
  public class SMMobileNote5SearchSignalStrengthTests extends SMSamsungGalaxyNote5PhoneBaseClass {
	static String sunit = "1005";
	static String curmod = "SMMobileNote5SearchSignalStrengthTests";
	
	
	@Test
	public void sMMobileNote5SearchSignalStrengthTests() {

		System.out.println("Starting : " + curmod);
			
		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
		sleep(999);

		
			
			// Select Units page
			sleep(6000);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView\r\n")).click();
			System.out.println("Select Unit page.. !");
			
			// Select seatch spy glass
			sleep(6000);
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]\r\n")).click();
			System.out.println("Select spy glass search.. !");
			
			
			// Select Enter search criteria 
			sleep(6000);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(sunit);
			System.out.println("Enter unit to search for: " +  sunit);
			
			
			// Select Enter search criteria 
			sleep(6000);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView\r\n")).click();
			System.out.println("View unit info.. !");
			sleep(3000);
			driver.navigate().back();
			sleep(3000);
			driver.navigate().back();
			sleep(3000);
			driver.navigate().back();
			
	
			sleep(1800);
			System.out.println("End: " + curmod);

			
	}

}
