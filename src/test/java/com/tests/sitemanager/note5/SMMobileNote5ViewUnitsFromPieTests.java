package com.tests.sitemanager.note5;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileNote5ViewUnitsFromPieTests extends SMSamsungGalaxyNote5EmailBaseClass {

	
	
	
	@Test
	public void sMMobileNote5ViewUnitsFromPieTests() {
		
		
		sleep(2000);
		System.out.println("Starting SMMobileNote5ViewUnitsFromPieTests.");
		// Skip tutorial
		sleep(3999);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(3999);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(3999);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

		System.out.println("Site Manager Home Page..!");
		sleep(3999);


		// Change to Noke Office Test
		ChangeToNokeOfficeTest();

		
		
		
		// Enter select pie chart
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView")).click();
		System.out.println("Select Pie chart.. !");
		sleep(2900);
		
		
		// Enter select units
		sleep(3999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button")).click();
		sleep(3000);
		System.out.println("Select Units to display.. !");
			

		// Exit to home page
		homepagetoexit();
				
		sleep(2000);
		System.out.println("End SMMobileNote5ViewUnitsFromPieTests.");

		
	
		}
	
	}
		



	



