package com.tests.sitemanager.note5;


import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileNote5RemoteUnlockAvailableUnitsTests extends SMSamsungGalaxyNote5EmailBaseClass {
	static String curmod = "SMMobileNote5RemoteUnlockAvailableUnitsTests";
	static String sunit = "1001";
	
	
	
	@Test
	public void sMMobileNote5RemoteUnlockAvailableUnitsTests() {

		
		
		sleep(2900);
		System.out.println("Starting : " + curmod);

			
		// skit tutorial
		skipTutorial();
		sleep(999);
		
		// Select popup and Allow Access
		selectLocationAllowAccess();
		sleep(999);
	
		// Change to Noke Office Test
		ChangeToNokeOfficeTest();
			

			// Select View Units from menu
			sleep(4900);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
			System.out.println("Select Unit View.. !");
			
			// ***************************************************************
	
			
			// Select Units page
			sleep(5999);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Units\"]/android.widget.ImageView")).click();
			System.out.println("Select Unit page.. !");

			// Select search spy glass
			sleep(5999);
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"search button\"]")).click();
			System.out.println("Select spy glass search.. !");

			// Select Enter search criteria open list
			sleep(5999);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).sendKeys(sunit);
			System.out.println("Enter unit to search for.. !");

			
 			// Select Enter Unlock command 
			sleep(6999);
			driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.Button\r\n")).click();
			System.out.println("Select Unlock.. !");
			sleep(9999);
			System.out.println("Note: Lock on wall should be green..!" + sunit);
			driver.navigate().back();
			sleep(3999);
			driver.navigate().back();

			
			// Select home page
			sleep(3999);
			driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]/android.widget.ImageView\r\n")).click();
			System.out.println("Select and open Home page.. !");
		
			
			
			
			
		
			
			sleep(2900);
			System.out.println("Ending : " + curmod);

			
			

		}


	
	}


