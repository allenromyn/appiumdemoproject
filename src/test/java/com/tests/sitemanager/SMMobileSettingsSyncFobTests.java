package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class SMMobileSettingsSyncFobTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileSettingsSyncFobTests extends SMSamsungGalaxyG9BaseClass {
	static String curmod  = "SMMobileSettingsSyncFobTests";
	
	
	@Test
	public void sMMobileSettingsSyncFobTests() {
		System.out.println("Starting :" + curmod + " test.");
		System.out.println("***************************************");
		
	   	// Skip tutorial 
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
	
	
		// Go to exit page to logout / Bottom menu
		WebDriverWait waitHomeExit = new WebDriverWait(driver, 20);
		waitHomeExit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		System.out.println("Oprn Exit page / Lower Menu .. !");
		sleep(2999);

		
		// Access exit page menu select advanced settings
		sleep(999);
		WebDriverWait waitSyncFobLink = new WebDriverWait(driver, 20);
		waitSyncFobLink.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Sync Fob for Tenant..!");	
		sleep(2999);		  
		driver.navigate().back();
		
		
		// Exit to home page
		// Select home from lower menu 
		System.out.println("Select Home icon  - viw lower menu.. !");
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"More\"]/android.widget.ImageView\r\n")).click();
		sleep(2999);	
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
			
	}

	
}


	


