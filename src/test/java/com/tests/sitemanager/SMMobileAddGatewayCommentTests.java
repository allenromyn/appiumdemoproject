package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class SMMobileAddGatewayCommentTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileAddGatewayCommentTests";
	static String comment = "This is a comment added by automation.";
	
	
	@Test
	public void sMMobileAddGatewayCommentTests() {
	
		
		System.out.println("Starting :" + curmod + " test.");
		System.out.println("***************************************");
		// Login and setup from extended class.
		//**************************************************
		// login bar, login page - from baseclass.
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();
		// biometricsApproval << located in base class , this is an if statement.

		// Select Bottom menu to 
		selectMoreLowerRight();
		
		// Select Add / Gateways 
		sleep(1999);
		WebDriverWait waitAddGateway = new WebDriverWait(driver, 20);
		waitAddGateway.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Add Gateways.");
		
		
		// Allow Storage Smart Entry to take pictures and record video?
		sleep(1999);
		WebDriverWait waitAllowStorage = new WebDriverWait(driver, 20);
		waitAllowStorage.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n")).click();
		System.out.println("Select Allow Storage Smart Entry to take pictures and record video?");
		sleep(9999);
		driver.navigate().back();

		
		
//		// Enter comment !
//		WebDriverWait waitEnterComment = new WebDriverWait(driver, 20);
//		waitEnterComment.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.EditText\r\n")));
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.EditText\r\n")).sendKeys(comment);
//		System.out.println("Enter comment, from automation.");
//		sleep(3999);
		
		
		// Save and return to home page.
		WebDriverWait waitSelectSave = new WebDriverWait(driver, 20);
		waitSelectSave.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button[1]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button[1]\r\n")).click();
		System.out.println("Select Save and Finish.");
		sleep(3999);

		
		

		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
			
	}


}


 
