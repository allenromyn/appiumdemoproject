package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
public class SMMobileHelpSupportLinkTests extends SMSamsungGalaxyG9BaseNewLoginClass {
//public class SMMobileHelpSupportLinkTests extends SMSamsungGalaxyG9BaseClass {
	String curmod = "SMMobileHelpSupportLinkTests";

	@Test
	public void sMMobileHelpSupportLinkTests() throws Exception {

		System.out.println("Starting :" + curmod + " test.");
		
		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();


		System.out.println("Site Manager Home Page..!");
		// Change test location
		changeTestLocation();
		
		// Access lower menu 
		selectMoreLowerRight();

		
		// Access Help and Support
		selectHelpandSupport();
		System.out.println("Select Help & Support Link  .. !");

		// Access Help and Support FAQ
		WebDriverWait waitFaq = new WebDriverWait(driver, 20);
		waitFaq.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]")));
		MobileElement selecthelpandsupportfaq = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
		selecthelpandsupportfaq.click();
		System.out.println("Select FAQ Link  .. !");
		sleep(3999);
		driver.navigate().back();
		sleep(2999);
		// Access Help and Support
		selectHelpandSupport();

		// Access Help and Support View Tenant Support
		WebDriverWait waitViewTenant = new WebDriverWait(driver, 20);
		waitViewTenant.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]")).click();
		System.out.println("Select View Tenant Support Link  .. !");

		
		
		
		// Access Help and Support View Tenant Support start here.
		WebDriverWait waitStartHere = new WebDriverWait(driver, 20);
		waitStartHere.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]")).click();
		System.out.println("Select View Tenant Support-Start Here Link  .. !");
		sleep(2999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Open Gate Door.
		WebDriverWait waitOpenGate = new WebDriverWait(driver, 20);
		waitOpenGate.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]")).click();
		System.out.println("Select View Tenant Support-Open Gate Door Link  .. !");
		sleep(2999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Unlocking your unit
		WebDriverWait waitUnlockingUnit = new WebDriverWait(driver, 20);
		waitUnlockingUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]")).click();
		System.out.println("Select View Tenant Support-Unlocking Your Unit  .. !");
		sleep(2999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support locking your unit
		WebDriverWait waitlockingUnit = new WebDriverWait(driver, 20);
		waitlockingUnit.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]")).click();
		System.out.println("Select View Tenant Support-Locking Your Unit  .. !");
		sleep(2999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Didgital Key
		WebDriverWait waitDigitalKey = new WebDriverWait(driver, 20);
		waitDigitalKey.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]")).click();
		System.out.println("Select View Tenant Support-Sharing a Digital Key  .. !");
		sleep(2999);
		driver.navigate().back();

		// Access Help and Support View Tenant Support Sharing a Didgital Key
		WebDriverWait waitContactUs = new WebDriverWait(driver, 20);
		waitContactUs.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[7]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[7]\r\n")).click();
		System.out.println("Select Contact Us  .. !");
		// Select clear contact us popup
		sleep(3999);
		WebDriverWait waitContactUsPopup = new WebDriverWait(driver, 20);
		waitContactUsPopup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[4]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[4]\r\n")).click();
		System.out.println("Cancel Contact Us popup.. !");
		sleep(2999);
		driver.navigate().back();


		// Exit to home page
		// Select home from lower menu 
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(2999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();				
			
	}




























	public void selectHelpandSupport() {
		sleep(3999);
		WebDriverWait waitHelpSupportLink = new WebDriverWait(driver, 20);
		waitHelpSupportLink.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[9]/android.widget.ImageView\r\n")).click();
	}
	



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void locationAccessOK() {
		// Select and set location popup
		sleep(4999);
		MobileElement widgetclick2 = driver.findElement(By.id("android:id/button1"));
		widgetclick2.click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(4999);
		MobileElement allowaccesslocation = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]\r\n"));
		allowaccesslocation.click();
		System.out.println("Allow access to application.. !");
	}
}
