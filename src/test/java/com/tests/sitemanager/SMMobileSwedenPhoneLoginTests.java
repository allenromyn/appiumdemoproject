package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;

import java.io.IOException;


// public class SMMobileSwedenPhoneLoginTests extends SMSamsungGalaxyG9BaseClass {	
public class SMMobileSwedenPhoneLoginTests extends SMSamsungGalaxyG9BaseNewLoginClass {	
	static String country1 = "Sweden";
	static String ph       = "771793336";
	static String curmod = "SMMobileSwedenPhoneLoginTests"; 
	@Test
	public void sMMobileSwedenPhoneLoginTests() throws IOException  {
		

		System.out.println("Starting :" + curmod + " test.");
		sleep(4999);		
		
		// Select and change to dev environment popup
		sleep(4999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout")).click();
		
		// Click dev change to dev environment
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Environment changed.");
		sleep(4999);
		
		// Select country dropdown button
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.ImageView[2]\r\n")).click();
		System.out.println("Select Country dropdown..!");
		sleep(4999);
		

		
		
		// Select Search for country. ( Sweden )
		
		MobileElement searchenter =  driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]\r\n"));
		searchenter.click();
		sleep(3999);
		searchenter.clear();
		searchenter.sendKeys(country1);
		sleep(3999);
		System.out.println("Enter search for country..!");
		sleep(4999);
		
				
		// Select  country flag/dropdown
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView\r\n")).click();
		System.out.println("Select country..!");
		sleep(3000);		
		
		
	
		// select phone enter text line
		sleep(1500);
		MobileElement phoneinputline = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.TextView\r\n"));
		phoneinputline.clear();
		sleep(1000);
		phoneinputline.sendKeys(ph);
		sleep(1000);
		System.out.println("Entetr phone."+ ph + "  ..!" );
		sleep(2000);
	
		
		
		// enter password
		sleep(3000);
		driver.findElement(By.id("com.noke.nokeaccess:id/password_edit_text")).sendKeys(pw);
		System.out.println("Enter password.");
		
		
		// login arrow
		sleep(2000);
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Show password\"]")).click();
		System.out.println("login ..!" );

		// Skip tutorial
		sleep(5000);
		driver.findElement(By.id("com.noke.nokeaccess:id/skip_text")).click();
		System.out.println("Skip tutorial.");

		// Select and set location popup
		sleep(5000);
		driver.findElement(By.id("android:id/button1")).click();
		System.out.println("Select Ok accept location popup.");

		// Select Allow access
		sleep(5000);
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		System.out.println("Allow access to application.. !");

	
		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// homepagetoexit();
			
	}

	
}

