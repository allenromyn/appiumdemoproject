package com.tests.sitemanager;

import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class SMMobileEmailAddGatewayTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod  = "SMMobileEmailAddGatewayTests";
	
	
	@Test
	public void sMMobileEmailAddGatewayTests() {

		System.out.println("Starting :" + curmod + " test.");
		System.out.println("***************************************");
		// Login and setup from extended class.
		//**************************************************

		// login bar, login page - from baseclass.
		
		// skip tutorial
		skipTutorial();
		//location and access popups
		locationAccessDevice();
		// Change test location " Office Overstock Storage 
		changeTestLocation();
		// biometricsApproval << located in base class , this is an if statement.

		// Select Bottom menu to 
		selectMoreLowerRight();

		// Access exit page menu select Add Gateway // //
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Add Gateway.. !");;
		// Select allow take picture - camera link
		sleep(5999);
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[2]\r\n")).click();
		System.out.println("Select Deny  Smart Entry to take pictures.. !");
		sleep(5999);
		driver.navigate().back();

		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();
		

	}

}
