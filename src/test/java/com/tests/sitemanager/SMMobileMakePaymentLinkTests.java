package com.tests.sitemanager;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class SMMobileMakePaymentLinkTests extends SMSamsungGalaxyG9BaseNewLoginClass {
	static String curmod = "SMMobileMakePaymentLinkTests";
	
	@Test
	public void sMMobileMakePaymentLinkTests() {

		System.out.println("Starting :" + curmod + " test.");
		System.out.println("******************************");


		
	   	// Skip tutorial 
		skipTutorial();

		//location and access popups
		locationAccessDevice();

		System.out.println("Site Manager Home Page..!");
		// Change test location
		sleep(2999);
		changeTestLocation();


		// Access lower menu 
		selectMoreLowerRight();

		// Access Help and Support
		sleep(3999);
		WebDriverWait waitHelpSupportLink = new WebDriverWait(driver, 20);
		waitHelpSupportLink.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[10]/android.widget.ImageView\r\n")).click();
		System.out.println("Select Help & Support Link  .. !");

		// Access Help and Support View Tenant Support
		sleep(3999);
		WebDriverWait waitViewTenantSupportLink = new WebDriverWait(driver, 20);
		waitViewTenantSupportLink.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView[3]\r\n")).click();
		System.out.println("Select View Tenant Support Link  .. !");

		// Access Help and Support View Tenant Support Sharing a Make a Payment
		sleep(999);
		WebDriverWait waitHelpSupportPaymentLink = new WebDriverWait(driver, 20);
		waitHelpSupportPaymentLink.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]\r\n")));
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[6]\r\n")).click();
		System.out.println("Select View Tenant Support-Making A Payment- currently links to paypal login page.. !");
		sleep(3999);
		driver.navigate().back();
		sleep(3999);
		driver.navigate().back();



		// Exit to home page
		System.out.println("Ending :" + curmod + " test.");
		System.out.println("***************************************");
		sleep(1999);
		// Select More - lower right corner - Exit swipe up - slide 
		selectMoreLowerRight();
		// Swipe up to locate logout link.
		swipeBottomToTop();
		// Select logout
		logoutWithPopup();

		
	}
	
}
